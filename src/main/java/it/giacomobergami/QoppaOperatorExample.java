package it.giacomobergami;

import it.giacomobergami.grammarSolver.GrammarSolver;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.semanticGraph.StanfordGraph;
import it.giacomobergami.semanticGraph.patterns.ChangeWith;
import it.giacomobergami.semanticGraph.patterns.MoveDeterminerPattern;
import it.giacomobergami.semanticGraph.patterns.RemovePunctuation;

import java.io.IOException;

public class QoppaOperatorExample {



    public static void main(String args[]) throws IOException {
        //Provide a query as a full text example
        String query = "The query groups all the authors by institution and return their cooperation network";

        /*
         * DATA (1)
         *
         * Parses the query into an intermediate representation
         */
        NestedGraph dataGraph = new StanfordGraph()
                .parse(query)
                .getGraph();


        /*
         * PROCESSING (2)
         */
        /*MultipleTransformingQuery mrs = new MultipleTransformingQuery()
                .addPattern(MoveDeterminerPattern.create())
                .addPattern(RemovePunctuation.create())
                .addPattern(Conjunction.create())*/;
        GrammarSolver gs = new GrammarSolver(JId.ONE);
        gs
                .add(RemovePunctuation.create())
                .add(MoveDeterminerPattern.create())
                .add(ChangeWith.create());

        //System.out.println("INPUT:\n"+dataGraph.toString());
        NestedGraph result = gs.apply(dataGraph);


        postProcess(result);

        System.out.println("OUTPUT:\n"+result.toString());

        /*FilterIterator<JPair<IdExpr, NestedGraph.Reference>> it = new FilterIterator<JPair<IdExpr, NestedGraph.Reference>>(result.iterateOverVertices()) {
            @Override
            public boolean test(JPair<IdExpr, NestedGraph.Reference> o) {
                return o.value.solve().hasLabel("operator");
            }
        };
        while (it.hasNext()) {
            JPair<IdExpr, NestedGraph.Reference> operator = it.next();
            if (operator.value.solve().get("lemma").key.toString().equals("select")) {
                Iterator<NestedGraph.Reference> selectIterator = operator.value.outEdges();
                while (selectIterator.hasNext()) {
                    //System.out.println(selectIterator.next().dst().solve().toString());
                }
            }
        }*/

        result.view();


        /*MultipleRuleSolver mrs = new MultipleRuleSolver()
                .addPattern(MoveDeterminerPattern.create())
                .addPattern(RemovePunctuation.create());

        NestedGraph result = mrs.apply(dataGraph);

        result.toGMLGraph("0.gml");*/
    }

    private static void postProcess(NestedGraph dataGraph) {
    }

}
