package it.giacomobergami.nestedGraph.repository;

import com.google.common.collect.HashMultimap;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.util.io.gml.GMLWriter;
import it.giacomobergami.nestedGraph.HVertex;
import it.giacomobergami.nestedGraph.queries.EmptyExpression;
import it.giacomobergami.nestedGraph.queries.ExpressionListExpr;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.queries.QExpression;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.utils.VoidIterator;
import it.giacomobergami.utils.states.TransformingState;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.view.Viewer;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NestedGraph implements INGraph, Gamma {

    HashMap<JId, HVertex> vertexContainer, edgeContainer;
    Map<JId, JPair<JId, JId>> src_dst_Edges;
    HashMultimap<JId, JId> vertexToOutgoingEdges, vertexToIngoingEdges;
    NestedGraph repoInternal;

    public void removeEdge(JId jId) {
        edgeContainer.remove(jId);
        JPair<JId, JId> element = src_dst_Edges.remove(jId);
        vertexToOutgoingEdges.remove(element.key, jId);
        vertexToIngoingEdges.remove(element.value, jId);
    }

    public void removeVertex(JId jId) {
        vertexContainer.remove(jId);
        {
            ArrayList<JId> ingoing = new ArrayList<>(vertexToIngoingEdges.get(jId));
            for (JId x : ingoing) {
                removeEdge(x);
            }
        }
        {
            ArrayList<JId> outgoing = new ArrayList<>(vertexToOutgoingEdges.get(jId));
            for (JId x : outgoing) {
                removeEdge(x);
            }
        }
    }

    public NestedGraph() {
        vertexContainer = new HashMap<>();
        edgeContainer = new HashMap<>();
        src_dst_Edges = new HashMap<>();
        vertexToOutgoingEdges = HashMultimap.create();
        vertexToIngoingEdges = HashMultimap.create();
        repoInternal = this;
    }

    private NestedGraph(HashMap<JId, HVertex> vertexContainer,
                       HashMap<JId, HVertex> edgeContainer,
                       Map<JId, JPair<JId, JId>> src_dst_Edges,
                       HashMultimap<JId, JId> vertexToOutgoingEdges,
                       HashMultimap<JId, JId> vertexToIngoingEdges) {
        this.vertexContainer = vertexContainer;
        this.edgeContainer = edgeContainer;
        this.src_dst_Edges = src_dst_Edges;
        this.vertexToOutgoingEdges = vertexToOutgoingEdges;
        this.vertexToIngoingEdges = vertexToIngoingEdges;
        repoInternal = this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<JId, HVertex> v_e : vertexContainer.entrySet()) {
                sb.append(v_e.getKey());
                sb.append(v_e.getValue());
        }
        for (Map.Entry<JId, HVertex> v_e : edgeContainer.entrySet()) {
                sb.append(v_e.getKey());
                JPair<JId, JId> sd = src_dst_Edges.get(v_e.getKey());
                sb.append('<').append(sd.key).append(',').append(sd.value).append('>');
                sb.append(v_e.getValue());
        }
        return sb.toString();
    }

    @Override
    public int vertexCount() {
        return vertexContainer.size();
    }

    public boolean contains(IdExpr id) {
        JId key = id.key;
        return id.value ?
                (vertexContainer.containsKey(key)) :
                (edgeContainer.containsKey(key))
                ;
    }

    @Override
    public Iterator<Reference> getIterator(IdExpr id) {
        Reference toret = get(id);
        return toret == null ? new VoidIterator<>() : Arrays.stream(new Reference[]{toret}).iterator();
    }

    @Override
    public void instantiateFromIterator(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns) {
        Reference toret = get(id);
        if (toret != null)
            resolveIdsWithPatterns.add(id);
    }

    public JId maxVertexId() {
        return  Collections.max(vertexContainer.keySet());
    }


    public class Reference {
        public final JId id;
        public final boolean isVertex;
        private final NestedGraph handle;

        @Override
        public String toString() {
            return (isVertex ? "V" : "E") + "Ref("+id+")";
        }

        public Reference(JId id, boolean isVertex, NestedGraph handle) {
            this.id = new JId(id);
            this.isVertex = isVertex;
            this.handle = handle;
        }

        public Reference(int id, boolean isVertex, NestedGraph handle) {
            this.id = new JId(id);
            this.isVertex = isVertex;
            this.handle = handle;
        }

        public Reference(long id, boolean isVertex, NestedGraph handle) {
            this((int)id, isVertex, handle);
        }

        public Reference(IdExpr x, NestedGraph handle) {
            this.id = x.key;
            this.isVertex = x.value;
            this.handle = handle;
        }

        public HVertex solve() {
            return isVertex ? handle.vertexContainer.get(id) : handle.edgeContainer.get(id);
        }

        public QExpression evaluateField(String field) {
            ExpressionListExpr<QExpression> elems = new ExpressionListExpr<>();
            solve().ifGetUpdateList(field, repoInternal, elems);
            return (elems.isEmpty())  ? EmptyExpression.ee : (elems.size() == 1 ? elems.get(0) : elems);
        }

        public <T> T evaluateField(String field,
                                   Function<QExpression, T> ifPresentSingle,
                                   Function<ExpressionListExpr<QExpression>,T> ifPresentCollection, T ifAbsent) {
            ExpressionListExpr<QExpression> elems = new ExpressionListExpr<>();
            solve().ifGetUpdateList(field, repoInternal, elems);
            return (elems.isEmpty())  ? ifAbsent :
                    (elems.size() == 1 ? ifPresentSingle.apply(elems.get(0)) :
                                         ifPresentCollection.apply(elems));
        }

       /* public Stream<Reference> unnest(String field) {
            return solve().get(field).value.stream().map(Reference::new);
        }*/

        public Reference src() {
            return new Reference(src_dst_Edges.get(id).key, true, handle);
        }

        public Reference dst() {
            return new Reference(src_dst_Edges.get(id).value, true, handle);
        }

        public Iterator<Reference> toIterate(HashMultimap<JId, JId> obj) {
            if (isVertex) {
                final Iterator<JId> it = obj.get(id).iterator();
                return new Iterator<Reference>() {
                    @Override
                    public boolean hasNext() {
                        return it.hasNext();
                    }
                    @Override
                    public Reference next() {
                        return new Reference(it.next(), false, handle);
                    }
                };
            }
            return new VoidIterator<>();
        }

        public Iterator<Reference> outEdges(){
            return toIterate(vertexToOutgoingEdges);
        }

        public Iterator<Reference> inEdges() {
            return toIterate(vertexToIngoingEdges);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Reference)) return false;

            Reference reference = (Reference) o;

            if (isVertex != reference.isVertex) return false;
            return id.equals(reference.id);
        }

        @Override
        public int hashCode() {
            int result = id.hashCode();
            result = 31 * result + (isVertex ? 1 : 0);
            return result;
        }

        public IdExpr asIdExpr() {
            return new IdExpr(id, isVertex, false);
        }

        public Reference setAndUpdate(HVertex solve, TransformingState c, Gamma dataGraph) {
            solve().copyFrom(solve).updateWithExternal(c, dataGraph);
            return this;
        }

        public Reference setAndUpdate(HVertex solve, Collection<TransformingState> c, Gamma dataGraph) {
            HVertex self = solve().copyFrom(solve);
            for (TransformingState x : c) {
                self.updateWithExternal(x, dataGraph);
            }
            return this;
        }

        public Reference setWithNoUpdate(HVertex solve) {
            solve().copyFrom(solve);
            return this;
        }
    }


    public Reference insertNewVertex() {
        JId id = new JId(vertexContainer.isEmpty() ? new JId(0) : Collections.max(vertexContainer.keySet()).add(JId.ONE));
        vertexContainer.put(id, new HVertex());
        return new Reference(id, true, this);
    }


    public Reference insertNewVertexWithForcedIndex(JId index) {
        JId id = new JId(index);
        vertexContainer.put(id, new HVertex());
        return new Reference(id, true, this);
    }

    public Reference insertNewEdge(JId src, JId dst) {
        JId id = new JId(edgeContainer.isEmpty() ? new JId(0) : Collections.max(edgeContainer.keySet()).add(JId.ONE));
        edgeContainer.put(id, new HVertex());
        Reference toret = new Reference(id, false, this);
        src_dst_Edges.put(id, new JPair<>(src, dst));
        vertexToOutgoingEdges.put(src, toret.id);
        vertexToIngoingEdges.put(dst, toret.id);
        return toret;
    }

    public Reference insertNewEdgeWithForcedIndex(JId index, JId src, JId dst) {
        JId id = new JId(index);
        edgeContainer.put(id, new HVertex());
        Reference toret = new Reference(id, false, this);
        src_dst_Edges.put(id, new JPair<>(src, dst));
        vertexToOutgoingEdges.put(src, toret.id);
        vertexToIngoingEdges.put(dst, toret.id);
        return toret;
    }

    @Override
    public Reference getRawVertex(JId x) {
        return vertexContainer.containsKey(x) ? new Reference(x, true, this) : null;
    }

    @Override
    public Reference getRawEdge(JId x) {
        return edgeContainer.containsKey(x) ? new Reference(x, false, this) : null;
    }


    public Reference get(IdExpr id) {
        return id.value ? getRawVertex(id.key) : getRawEdge(id.key);
    }

    public Reference get(JId x, boolean isVertex) {
        return isVertex ? getRawVertex(x) : getRawEdge(x);
    }

    public Iterator<JPair<IdExpr, Reference>> iterateOverVertices() {
        final Iterator<JId> it = vertexContainer.keySet().iterator();
        return new Iterator<JPair<IdExpr, Reference>>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public JPair<IdExpr, Reference> next() {
                JId idx = it.next();
                return new JPair<>(new IdExpr(idx,true,false),new Reference(idx,true,repoInternal));
            }
        };
    }

    public Iterator<JPair<IdExpr, Reference>> iterateOverEdges() {
        final Iterator<JId> it = edgeContainer.keySet().iterator();
        return new Iterator<JPair<IdExpr, Reference>>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public JPair<IdExpr, Reference> next() {
                JId idx = it.next();
                return new JPair<>(new IdExpr(idx,false,false),new Reference(idx,false,repoInternal));
            }
        };
    }

    public void toGMLGraph(String filename) throws IOException {
        TinkerGraph blueprintGraph = new TinkerGraph();
        Set<String> hs = new HashSet<>();
        for (Map.Entry<JId, HVertex> v_e : vertexContainer.entrySet()) {
            String x = v_e.getValue().get("value").toString();
            if (hs.add(x)) {
                blueprintGraph.addVertex(x);
            }
        }
        for (Map.Entry<JId, HVertex> v_e : edgeContainer.entrySet()) {
            JPair<JId, JId> sd = src_dst_Edges.get(v_e.getKey());
            if (vertexContainer.get(sd.key) != null && vertexContainer.get(sd.value) != null) {
                String src = vertexContainer.get(sd.key).get("value").toString(),
                        dst = vertexContainer.get(sd.value).get("value").toString();
                if (blueprintGraph.getVertex(src) != null && blueprintGraph.getVertex(dst) != null)  {
                    blueprintGraph.addEdge(v_e.getKey(), blueprintGraph.getVertex(src), blueprintGraph.getVertex(dst), v_e.getValue().getLabels().iterator().next());
                }
            }
        }
        FileOutputStream fos = new FileOutputStream(filename);
        GMLWriter.outputGraph(blueprintGraph, fos);
    }

    // For Traversal

    public NestedGraph copyExcept(HashSet<Reference> vRef, HashSet<Reference> eRef) {
        HashMap<JId, HVertex> vertices, edges;
        Map<JId, JPair<JId, JId>> edgeMap;
        HashMultimap<JId, JId> vOut, vIn;
        NestedGraph repoInternal;
        vertices = new HashMap<>();
        edges = new HashMap<>();
        edgeMap = new HashMap<>();
        vOut = HashMultimap.create();
        vIn = HashMultimap.create();
        NestedGraph toreturn = new NestedGraph();
        edgeContainer.forEach((k,v) -> {
            JId kToSet = k.dovetailWithZero();
            if (!eRef.contains(new Reference(k, false,toreturn))) {
                JPair<JId, JId> sd = src_dst_Edges.get(k);
                if ((!vRef.contains(new Reference(sd.key, true,toreturn))) && (!vRef.contains(new Reference(sd.value, true,toreturn)))) {
                    edges.put(kToSet, v);
                    JPair<JId, JId> cp = src_dst_Edges.get(k);
                    edgeMap.put(kToSet, new JPair<>(cp.key.dovetailWithZero(), cp.value.dovetailWithZero()));
                    for (JId toDovetail : this.vertexToOutgoingEdges.get(k)) {
                        vOut.put(kToSet, toDovetail.dovetailWithZero());
                    }
                    for (JId toDovetail : this.vertexToOutgoingEdges.get(k)) {
                        vIn.put(kToSet, toDovetail.dovetailWithZero());
                    }
                }
            }
        });
        vertexContainer.forEach((k,v) -> {
            if (!vRef.contains(new Reference(k,true,toreturn))) {
                vertices.put(k.dovetailWithZero(), v);
            }
        });
        toreturn.vertexContainer = vertices;
        toreturn.edgeContainer = edges;
        toreturn.src_dst_Edges = edgeMap;
        toreturn.vertexToOutgoingEdges = vOut;
        toreturn.vertexToIngoingEdges = vIn;
        return toreturn;
    }

    public void view() {
        MultiGraph g = new MultiGraph("plot");
        g.addAttribute("ui.stylesheet","edge { arrow-shape: arrow; }");
        Set<String> hs = new HashSet<>();
        for (Map.Entry<JId, HVertex> v_e : vertexContainer.entrySet()) {
            String x = v_e.getValue().get("value").toString();
            g.addNode(v_e.getKey().toString()).addAttribute("ui.label",x);
        }
        for (Map.Entry<JId, HVertex> v_e : edgeContainer.entrySet()) {
            JPair<JId, JId> sd = src_dst_Edges.get(v_e.getKey());
            if (vertexContainer.get(sd.key) != null && vertexContainer.get(sd.value) != null) {
                String src = vertexContainer.get(sd.key).get("value").toString(),
                        dst = vertexContainer.get(sd.value).get("value").toString();
                g.addEdge(v_e.getKey().toString(), sd.key.toString(), sd.value.toString(),true)
                        .addAttribute("ui.label",v_e.getValue().getLabels().stream().map(Object::toString).collect(Collectors.joining()));
            }
        }
        Viewer viewer = g.display();
        viewer.disableAutoLayout();
        viewer.enableAutoLayout();
    }


}
