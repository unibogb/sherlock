package it.giacomobergami.nestedGraph.repository;

import com.google.common.collect.HashMultimap;
import it.giacomobergami.nestedGraph.queries.IdExpr;

import java.util.Iterator;

public class PatternNGraphWithMorphism {

    HashMultimap<IdExpr, IdExpr> dataGraphToPatternGraphMorphism_inverseFunction;
    final PatternNGraph patternGraph;
    final NestedGraph dataGraph;

    public PatternNGraphWithMorphism(PatternNGraph patternGraph, NestedGraph dataGraph) {
        this.patternGraph = patternGraph;
        this.dataGraph = dataGraph;
        dataGraphToPatternGraphMorphism_inverseFunction = HashMultimap.create();
    }

    public void associateDataToPattern(IdExpr dataId, IdExpr patternId) {
        dataGraphToPatternGraphMorphism_inverseFunction.put(patternId, dataId);
    }


    public Iterator<NestedGraph.Reference> solveMorphismFor(IdExpr id) {
        Iterator<IdExpr> it = dataGraphToPatternGraphMorphism_inverseFunction.get(id).iterator();
        return new Iterator<NestedGraph.Reference>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public NestedGraph.Reference next() {
                return dataGraph.get(it.next());
            }
        };
    }
}
