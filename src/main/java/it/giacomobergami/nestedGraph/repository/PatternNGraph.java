package it.giacomobergami.nestedGraph.repository;

import com.google.common.collect.HashMultimap;
import it.giacomobergami.grammarSolver.ObjectPredicate;
import it.giacomobergami.nestedGraph.HVertex;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.utils.states.TransformingState;

import java.util.Iterator;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Predicate;


/*
 * Assumption: in a pattern graph there are no empty vertices, that is everything is "compact"
 */
public class PatternNGraph extends NestedGraph {

    HashMultimap<JPair<IdExpr, IdExpr>, BiFunction<Reference, Reference, Boolean>> hmm = HashMultimap.create();


    public Iterator<JPair<IdExpr, ObjectPredicate>> iterateOverVertexPredicates() {
        final Iterator<Map.Entry<JId, HVertex>> it = vertexContainer.entrySet().iterator();
        return new Iterator<JPair<IdExpr, ObjectPredicate>>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public JPair<IdExpr, ObjectPredicate> next() {
                Map.Entry<JId, HVertex> entry = it.next();
                return new JPair<>(new IdExpr(entry.getKey(),true,true),entry.getValue().getPredicate());
            }
        };
    }

    public Iterator<JPair<IdExpr, ObjectPredicate>> iterateOverEdgePredicates() {
        final Iterator<Map.Entry<JId, HVertex>> it = edgeContainer.entrySet().iterator();
        return new Iterator<JPair<IdExpr, ObjectPredicate>>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }
            @Override
            public JPair<IdExpr, ObjectPredicate> next() {
                Map.Entry<JId, HVertex> entry = it.next();
                return new JPair<>(new IdExpr(entry.getKey(),true,true),entry.getValue().getPredicate());
            }
        };
    }

    public Reference insertVertexQuery(Predicate<Reference> o, boolean isSingleInstance) {
        Reference j = super.insertNewVertex();
        ObjectPredicate prop = ObjectPredicate.create(o, isSingleInstance);
        j.solve().setPredicate(prop);
        return j;
    }

    public Reference insertEdgeQuery(Predicate<Reference> o, boolean isSingleInstance, JId src, JId dst) {
        Reference j = super.insertNewEdge(src, dst);
        ObjectPredicate prop = ObjectPredicate.create(o, isSingleInstance);
        j.solve().setPredicate(prop);
        return j;
    }

    public Reference insertEdgeQuery(Predicate<Reference> referencePredicate, boolean isSingleInstance, Reference vqWithDetAss, Reference vqDet) {
        return insertEdgeQuery(referencePredicate, isSingleInstance, vqWithDetAss.id, vqDet.id);
    }

    public boolean testJoinConditions(TransformingState state) {
        if (hmm.isEmpty()) return true;
        else {
            Iterator<Map.Entry<JPair<IdExpr, IdExpr>, BiFunction<Reference, Reference, Boolean>>> it = hmm.entries().iterator();
            boolean test = true;
            while (test && it.hasNext()) {
                Map.Entry<JPair<IdExpr, IdExpr>, BiFunction<Reference, Reference, Boolean>> current = it.next();
                Iterator<Reference> itLeft = state.getIterator(current.getKey().key);
                while (test && itLeft.hasNext()) {
                    Reference left = itLeft.next();
                    Iterator<Reference> itRight = state.getIterator(current.getKey().value);
                    while (test && itRight.hasNext()) {
                        Reference right = itRight.next();
                        test = test && current.getValue().apply(left, right);
                    }
                }
            }
            return test;
        }
    }

    public void addJoinCondition(Reference vqDet, Reference vqNext, BiFunction<Reference,Reference,Boolean> o) {
        hmm.put(new JPair<>(vqDet.asIdExpr(), vqNext.asIdExpr()), o);
    }
}
