package it.giacomobergami.nestedGraph.repository;

import it.giacomobergami.grammarSolver.ObjectPredicate;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.nestedGraph.utils.JPair;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Assumption: if the vertex has the same id as the pattern vertex, then they are the same vertex, and hence the
 *             morphism has to be resolved from the data graph through the instantiated patterns
 */
public class TransformNGraph implements INGraph {

    private PatternNGraphWithMorphism inverseMorphism;
    private final PatternNGraph actualTransformationGraph;

    public TransformNGraph() {
        this.inverseMorphism = null;
        actualTransformationGraph = new PatternNGraph();
    }

    public Iterator<JPair<IdExpr, ObjectPredicate>> iterateOverVertexPredicates() {
        return actualTransformationGraph.iterateOverVertexPredicates();
    }

    public Iterator<JPair<IdExpr, ObjectPredicate>> iterateOverEdgePredicates() {
        return actualTransformationGraph.iterateOverEdgePredicates();
    }

    public void setMorphism(PatternNGraphWithMorphism inverseMorphism) {
        this.inverseMorphism = inverseMorphism;
    }

    /**
     * Returns all the associations from that id
     * @param id
     * @return
     */
    public Iterator<NestedGraph.Reference> get(IdExpr id) {
        if (id.isQuery && actualTransformationGraph.contains(id)) {
            return inverseMorphism.solveMorphismFor(id);
        }
        return Arrays.stream(new NestedGraph.Reference[]{(id.value ? getRawVertex(id.key) : getRawEdge(id.key))}).iterator();
    }

    @Override
    public int vertexCount() {
        return actualTransformationGraph.vertexCount();
    }

    public NestedGraph.Reference insertNewVertex() {
        return actualTransformationGraph.insertNewVertex();
    }

    public NestedGraph.Reference insertNewVertexWithForcedIndex(int index) {
        return actualTransformationGraph.insertNewVertexWithForcedIndex(new JId(index));
    }

    public NestedGraph.Reference insertNewEdge(JId src, JId dst) {
        return actualTransformationGraph.insertNewEdge(src, dst);
    }

    @Override
    public NestedGraph.Reference getRawVertex(JId x) {
        return actualTransformationGraph.getRawVertex(x);
    }

    @Override
    public NestedGraph.Reference getRawEdge(JId x) {
        return actualTransformationGraph.getRawEdge(x);
    }

    public NestedGraph.Reference holdVertex(NestedGraph.Reference vqWithDetAss) {
        return actualTransformationGraph
                .insertNewVertexWithForcedIndex(vqWithDetAss.id)
                .setWithNoUpdate(vqWithDetAss.solve());
    }

    public NestedGraph.Reference holdEdge(NestedGraph.Reference vqWithDetAss) {
        return actualTransformationGraph
                .insertNewEdgeWithForcedIndex(vqWithDetAss.id, vqWithDetAss.src().id, vqWithDetAss.dst().id)
                .setWithNoUpdate(vqWithDetAss.solve());
    }

    @Override
    public String toString() {
        return actualTransformationGraph.toString();
    }

    public NestedGraph.Reference insertNewEdgeWithForcedIndex(int i, JId src, JId dst) {
        return actualTransformationGraph.insertNewEdgeWithForcedIndex(new JId(i), src, dst);
    }
}
