package it.giacomobergami.nestedGraph.repository;

import it.giacomobergami.nestedGraph.utils.JId;

public interface INGraph {
    int vertexCount();
    //NestedGraph.Reference insertNewVertex();
    //NestedGraph.Reference insertNewVertexWithForcedIndex(int index);
    //NestedGraph.Reference insertNewEdge(Long src, Long dst);
    NestedGraph.Reference getRawVertex(JId x);
    NestedGraph.Reference getRawEdge(JId x);
}
