package it.giacomobergami.nestedGraph.repository;

import it.giacomobergami.nestedGraph.queries.ExpressionListExpr;
import it.giacomobergami.nestedGraph.queries.IdExpr;

import java.util.Collection;
import java.util.Iterator;

public interface Gamma {
    public Iterator<NestedGraph.Reference> getIterator(IdExpr id);
    void instantiateFromIterator(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns);
}
