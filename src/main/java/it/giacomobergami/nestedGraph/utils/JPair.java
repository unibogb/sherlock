package it.giacomobergami.nestedGraph.utils;

public class JPair<K,V>  {
    public K key;
    public V value;

    public JPair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return key+"⇒"+value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JPair<?, ?> jPair = (JPair<?, ?>) o;

        if (key != null ? !key.equals(jPair.key) : jPair.key != null) return false;
        return value != null ? value.equals(jPair.value) : jPair.value == null;
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
