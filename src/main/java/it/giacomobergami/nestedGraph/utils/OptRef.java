package it.giacomobergami.nestedGraph.utils;

import java.util.function.Function;
import java.util.function.Supplier;

public class OptRef<T> {

    boolean isPresent;
    T object;

    public OptRef() {
        isPresent = false;
        object = null;
    }

    public OptRef(T element) {
        isPresent = true;
        object = element;
    }

    public T unsafeGet() {
        return object;
    }

    public boolean isPresent() {
        return isPresent;
    }

    public <K> K  elim(Function<T, K> ifPresent, Supplier<K> ifNotPresent) {
        return isPresent ? ifPresent.apply(object) : ifNotPresent.get();
    }

    public <K> K  elim(Function<T, K> ifPresent, K ifNotPresent) {
        return isPresent ? ifPresent.apply(object) : ifNotPresent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptRef)) return false;

        OptRef<?> that = (OptRef<?>) o;

        if (isPresent != that.isPresent) return false;
        return object != null ? object.equals(that.object) : that.object == null;
    }

    @Override
    public int hashCode() {
        int result = (isPresent ? 1 : 0);
        result = 31 * result + (object != null ? object.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return isPresent ? object.toString() : "null";
    }
}
