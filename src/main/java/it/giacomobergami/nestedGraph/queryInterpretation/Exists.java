package it.giacomobergami.nestedGraph.queryInterpretation;

import it.giacomobergami.nestedGraph.repository.NestedGraph;

import java.util.Iterator;
import java.util.function.Predicate;

public abstract class Exists implements Predicate<Iterator<NestedGraph.Reference>> {
    @Override
    public boolean test(Iterator<NestedGraph.Reference> referenceIterator) {
        while (referenceIterator.hasNext()) {
            if (testEdge(referenceIterator.next())) {
                return true;
            }
        }
        return false;
    }

    public abstract boolean testEdge(NestedGraph.Reference next);
}

