package it.giacomobergami.nestedGraph;

import it.giacomobergami.grammarSolver.ObjectPredicate;
import it.giacomobergami.nestedGraph.queries.*;
import it.giacomobergami.nestedGraph.repository.Gamma;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.JId;

import java.util.*;

public class HVertex {
    private Set<String> labelSet;
    private Map<String, Value> attributeValue;
    private ObjectPredicate asPredicate;
    private boolean transformation = false;

    public HVertex() {
        labelSet = new HashSet<>();
        attributeValue = new HashMap<>();
        asPredicate = ObjectPredicate.defaultPredicate;
    }

    public boolean isEmpty() {
        return labelSet.isEmpty() && attributeValue.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[\n\tlabels={");
        Iterator<String> labelIterator = labelSet.iterator();
        while (labelIterator.hasNext()) {
            sb.append(labelIterator.next());
            if (labelIterator.hasNext()) sb.append(", ");
        }
        sb.append("}\n\tvalues={");
        Iterator<Map.Entry<String, Value>> valueIterator = attributeValue.entrySet().iterator();
        while (valueIterator.hasNext()) {
            Map.Entry<String, Value> nx = valueIterator.next();
            sb.append(nx.getValue()+" : "+nx.getKey());
            if (valueIterator.hasNext()) sb.append(", ");
        }
        sb.append("}\n]\n");
        return sb.toString();
    }

    public HVertex updateWithExternal(final Gamma g, final Gamma dataGraph) {
        attributeValue.forEach((k, type) -> {
            if (type instanceof FieldSetExpr) {
                // Updating just the field
                ((FieldSetExpr) type).updateWith(g, dataGraph);
            }
        });
        return this;
    }

    public void addPredicate(ObjectPredicate prop) {
        this.asPredicate = prop;
    }

    public void addLabel(String label) {
        labelSet.add(label);
    }

    public boolean hasLabel(String label) {
        return labelSet.contains(label);
    }

    public boolean hasLabel(Collection<String> labels) {
        return labelSet.containsAll(labels);
    }

    public HVertex updateWithAggregation(String attribute, AggregationExpr aggregationFunction) {
        attributeValue.put(attribute, new Value(aggregationFunction, new ExpressionListExpr()));
        return this;
    }

    public void update(String attribute, int value) {
        attributeValue.put(attribute, new Value(value));
    }

    public void update(String attribute, String value) {
        attributeValue.put(attribute, new Value(value));
    }

    public HVertex initializeWithList(String attribute) {
        attributeValue.putIfAbsent(attribute, new Value());
        return this;
    }

    public HVertex addInList(String attribute, int toAdd, boolean isVertex, boolean isQuery) {
        attributeValue.get(attribute).value.add(new IdExpr(toAdd,isVertex, isQuery));
        return this;
    }

    public HVertex addInList(String attribute, JId toAdd, boolean isVertex, boolean isQuery) {
        attributeValue.get(attribute).value.add(new IdExpr(toAdd, isVertex, isQuery));
        return this;
    }

    public ExpressionListExpr getList(String attribute) {
        return attributeValue.getOrDefault(attribute, Value.eval).value;
    }

    public Value get(String attribute) {
        return attributeValue.getOrDefault(attribute, Value.eval);
    }

    public void ifGetUpdateList(String attribute, Gamma gamma, ExpressionListExpr<? super QExpression> list) {
        Value v = attributeValue.get(attribute);
        if (v != null) {
            v.ifGetUpdateList(gamma, list);
        }
    }


    public ObjectPredicate getPredicate() {
        return asPredicate;
    }

    public void setPredicate(ObjectPredicate predicate) {
        this.asPredicate = predicate;
    }

    public Set<String> getLabels() {
        return labelSet;
    }

    /**
     * Copies the vertex from an external source
     * @param solve     External element to copy
     * @return          Reference to the updated object
     */
    public HVertex copyFrom(HVertex solve) {
        labelSet.addAll(solve.labelSet);
        attributeValue.putAll(solve.attributeValue);
        asPredicate = solve.asPredicate;
        return this;
    }

    /**
     * Sets the value from an external reference
     * @param localLabel    Name of the local label to be set
     * @param remoteLabel   Name of the remote label to be extracted
     * @param vqDet         Origin of the external reference
     * @return
     */
    public HVertex setFromExternal(String localLabel, String remoteLabel, NestedGraph.Reference vqDet) {
        transformation = true;
        attributeValue.put(localLabel,  new FieldSetExpr(new FieldGetExpr(remoteLabel), vqDet.asIdExpr()));
        return this;
    }

    public boolean isTransformation() {
        return transformation;
    }
}
