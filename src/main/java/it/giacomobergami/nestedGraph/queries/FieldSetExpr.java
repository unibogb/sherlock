package it.giacomobergami.nestedGraph.queries;


import it.giacomobergami.nestedGraph.repository.Gamma;
import it.giacomobergami.nestedGraph.utils.JId;

import java.util.HashSet;

public class FieldSetExpr extends Value implements QExpression  {

    public FieldSetExpr(FieldGetExpr fieldGetExpr, IdExpr... expr) {
        super(fieldGetExpr, expr);
    }

    @Override
    public JId numeric() {
        return JId.ZERO;
    }

    @Override
    public Type _case() {
        return Type.FieldSet;
    }

    @Override
    public int compareTo(QExpression qExpression) {
        int cmp = qExpression._case().compareTo(Type.FieldSet);
        if (cmp == 0) return valueCompare((Value) qExpression);
        return 0;
    }

    public void updateWith(Gamma gamma, Gamma dataGraph) {
        // Please note: this does not evaluate the arguments, but it istantiates it inside the elements
        Value resolve;
        // 1) resolving all the ids associated to the outside elements
        {

            HashSet<IdExpr> resolveIdsWithPatterns = new HashSet<>();

            //FieldSetExpr this_key = ((FieldSetExpr) this.key);

            // The query manifests the id of the pattern query. This id is then resolved through
            // the gamma with the binded elements
            ExpressionListExpr<IdExpr> idExprs = new ExpressionListExpr<>();
            for (IdExpr id : this.value) {
                gamma.instantiateFromIterator(id, resolveIdsWithPatterns);
            }
            idExprs.addAll(resolveIdsWithPatterns);

            // Now, getting the actual expression to be evaluated
            resolve = new Value(this.key, idExprs);
        }
        // 2) Since some of the elements can be removed, I prefer to store all the outcomes permanently. Retrieve the associated values
        ExpressionListExpr<? super QExpression> vl = new ExpressionListExpr<>();
        resolve.ifGetUpdateList(dataGraph, vl);
        if (vl.isEmpty()) {
            clear();
        } else if (vl.size() == 1) {
            QExpression expr = vl.get(0);
            switch (expr._case()) {
                case Id:
                case Value:
                    key = ((ValueExpr)expr);
                    value.clear();
                    break;
                default:
                    clear();
                    break;
            }
        } else {
            key = new ValueExpr(vl);
            value.clear();
        }
    }
}
