package it.giacomobergami.nestedGraph.queries;

import it.giacomobergami.nestedGraph.utils.JId;

public class FieldGetExpr implements QExpression {
    public final String field;

    @Override
    public JId numeric() {
        return JId.ZERO;
    }

    @Override
    public QExpression.Type _case() {
        return Type.FieldGet;
    }

    public FieldGetExpr(String field) {
        this.field = field;
    }

    @Override
    public int compareTo(QExpression qExpression) {
        int cmp = qExpression._case().compareTo(Type.FieldGet);
        return cmp == 0 ?
                field.compareTo(((FieldGetExpr) qExpression).field) :
                cmp;
    }

    @Override
    public String toString() {
        return "?."+field;
    }

}
