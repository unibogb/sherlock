package it.giacomobergami.nestedGraph.queries;

import it.giacomobergami.nestedGraph.utils.JId;

import java.util.Collections;

public class AggregationExpr implements QExpression {

    @Override
    public JId numeric() {
        return JId.ZERO;
    }

    @Override
    public QExpression.Type _case() {
        return Type.Aggregation;
    }

    @Override
    public int compareTo(QExpression qExpression) {
        int cmp = qExpression._case().compareTo(Type.Aggregation);
        if (cmp == 0) {
            AggregationExpr rhe = (AggregationExpr)qExpression;
            cmp = function.compareTo(rhe.function);
            return cmp == 0 ? toAggregate.compareTo(rhe.toAggregate) : cmp;
        } else return cmp;
    }

    public enum Aggregators {
        MIN,        // Minimum between all the elements
        MAX,        // Maximum between all the elements
        COUNT,      // Number of the stored elements
        SUM,        // Summation for any numeric
        TIMES,      // Multiplication for any numeric
        FIRST,      // Returns the first element of the collection
        LAST,        // Returns the last element of the collection
        CONCATENATE
    }

    public final Aggregators function;
    public final QExpression toAggregate;

    public AggregationExpr(Aggregators function, QExpression toAggregate) {
        this.function = function;
        this.toAggregate = toAggregate;
    }

    public QExpression evaluateGenericAggregation(ExpressionListExpr<? super QExpression> list) {
        switch (function) {
            case FIRST: {
                return list.isEmpty() ? EmptyExpression.ee : list.get(0);
            }
            case LAST: {
                return list.isEmpty() ? EmptyExpression.ee : list.get(list.size()-1);
            }
            case COUNT: {
                return new ValueExpr(list.size());
            }
            case MIN: {
                return Collections.min(list);
            }
            case MAX: {
                return Collections.max(list);
            }
            case SUM: {
                ValueExpr zero = new ValueExpr(0);
                for (QExpression x : list) {
                    if (Type.Value.equals(x._case())) {
                        zero.sumIfNumeric(((ValueExpr)x));
                    }
                }
                return zero;
            }

            case TIMES:{
                ValueExpr one = new ValueExpr(1);
                for (QExpression x : list) {
                    if (Type.Value.equals(x._case())) {
                        one.mulIfNumeric(((ValueExpr)x));
                    }
                }
                return one;
            }

            case CONCATENATE: {
                ValueExpr stringify = new ValueExpr("");
                stringify.openStringBuilder();
                for (QExpression x : list) {
                    if (Type.Value.equals(x._case())) {
                        stringify.appendIfString(((ValueExpr)x));
                    }
                }
                stringify.closeStringBuilder();
                return stringify;
            }
        }
        return EmptyExpression.ee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AggregationExpr that = (AggregationExpr) o;

        if (function != that.function) return false;
        return toAggregate != null ? toAggregate.equals(that.toAggregate) : that.toAggregate == null;
    }

    @Override
    public int hashCode() {
        int result = function != null ? function.hashCode() : 0;
        result = 31 * result + (toAggregate != null ? toAggregate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(function.name())
                .append('{')
                .append(toAggregate.toString())
                .append('}')
                .toString();
    }

}
