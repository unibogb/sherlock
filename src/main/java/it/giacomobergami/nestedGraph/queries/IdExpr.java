package it.giacomobergami.nestedGraph.queries;

import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.nestedGraph.utils.JPair;

public class IdExpr extends JPair<JId, Boolean> implements QExpression {

    // As it should be, this field is ignored during query comparison
    public final boolean isQuery;

    @Override
    public JId numeric() {
        return key;
    }

    @Override
    public QExpression.Type _case() {
        return Type.Id;
    }

    @Override
    public int compareTo(QExpression qExpression) {
        int cmp = qExpression._case().compareTo(Type.Id);
        if (cmp == 0) {
            cmp = key.compareTo(((IdExpr) qExpression).key);
            if (cmp == 0) cmp = value.compareTo(((IdExpr) qExpression).value);
            return cmp;
        }
        return cmp;
    }

    public IdExpr(int[] deserialize, boolean isVertex, boolean isQuery) { super(new JId(deserialize), isVertex);
        this.isQuery = isQuery;
    }
    public IdExpr(JId element, boolean isVertex, boolean isQuery) { super(element, isVertex);
        this.isQuery = isQuery;
    }
    public IdExpr(int value, boolean isVertex, boolean isQuery) { super(new JId(value), isVertex);
        this.isQuery = isQuery;
    }
    public IdExpr() {
        this(0,false,false);
    }


    public static final IdExpr BOGUS = new IdExpr();

    @Override
    public String toString() {
        return (isQuery ? "QueryE[[" : "") + (value ? "V(" : "E(") + key.toString() + (isQuery ? ")]]" : ")");
    }
}
