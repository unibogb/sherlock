package it.giacomobergami.nestedGraph.queries;

import it.giacomobergami.nestedGraph.utils.JId;

public class EmptyExpression implements QExpression {
    public static EmptyExpression ee = new EmptyExpression();
    private EmptyExpression() {}

    @Override
    public JId numeric() {
        return JId.ZERO;
    }

    @Override
    public QExpression.Type _case() {
        return Type.Empty;
    }

    @Override
    public int compareTo(QExpression qExpression) {
        return qExpression._case().compareTo(Type.Empty);
    }

    @Override
    public String toString() {
        return ".";
    }
}
