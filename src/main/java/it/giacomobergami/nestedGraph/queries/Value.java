package it.giacomobergami.nestedGraph.queries;

import com.sun.xml.internal.bind.v2.model.core.ID;
import it.giacomobergami.nestedGraph.repository.Gamma;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.nestedGraph.utils.JId;

import java.util.HashSet;

public class Value extends JPair<QExpression, ExpressionListExpr<IdExpr>>  {
    public final static Value eval = new Value();

    public Value(QExpression key, IdExpr... value) {
        super(key, new ExpressionListExpr<>(value));
    }
    public Value(QExpression key, ExpressionListExpr value) {
        super(key, value);
    }
    public Value(int numericValue) {
        this(new ValueExpr(numericValue), ExpressionListExpr.ele);
    }
    public Value() {
        this(EmptyExpression.ee, new ExpressionListExpr());
    }
    public Value(String stringValue) {
        this(new ValueExpr(stringValue), ExpressionListExpr.ele);
    }
    public Value(JId id) {
        this(new ValueExpr(id), ExpressionListExpr.ele);
    }

    public void ifGetUpdateList(Gamma gamma, ExpressionListExpr<? super QExpression> list) {
        switch (key._case()) {
            // Any simple value is directly addet to the list
            case Id:
            case Value: {
                list.add(key);
                break;
            }

            // If the value contains a list of identifiers, I have to add it recursively, too
            case Aggregation: {
                AggregationExpr ae = (AggregationExpr)key;

                // Evaluating the expression that appears internally to the aggregation over the values
                ExpressionListExpr<? super QExpression> internalList = new ExpressionListExpr<>();
                new Value(ae.toAggregate, value).ifGetUpdateList(gamma, internalList);

                // Aggregate the extracted values
                list.add(ae.evaluateGenericAggregation(internalList));
                break;
            }

            case Empty:
            case List: {
                list.addAll(value);
                break;
            }

            case FieldSet: {
                throw new RuntimeException("ERROR: reaching an unreachable part");
            }

            // If the aggregation function itself is a getter field, then I have to run it recursively on the underneath elements
            case FieldGet: {
                String nestedGet = ((FieldGetExpr)key).field;
                for (IdExpr id : value) {
                    gamma
                            .getIterator(id)
                            .forEachRemaining(c -> c.solve().ifGetUpdateList(nestedGet, gamma, list));
                }
                break;
            }
        }
    }

    protected void clear() {
        key = EmptyExpression.ee;
        value.clear();
    }


    public int valueCompare(Value x) {
        int cmp = key.compareTo(x.key);
        return cmp == 0 ? value.compareTo(x.value) : cmp;
    }
}
