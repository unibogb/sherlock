package it.giacomobergami.nestedGraph.queries;

import it.giacomobergami.nestedGraph.utils.JId;

public class ValueExpr implements QExpression {
    private String stringValue;
    private IdExpr numericValue;
    private StringBuilder sb;
    private ExpressionListExpr<? super QExpression> valueExpression;
    private ValueType type;

    enum ValueType {
        StringValue,
        NumericValue,
        ListValue
    }

    @Override
    public JId numeric() {
        return numericValue.numeric();
    }

    @Override
    public QExpression.Type _case() {
        return Type.Value;
    }

    public ValueType getValueType() {
        return type;
    }

    public ValueExpr(String s) {
        stringValue = s;
        numericValue = null;
        sb = null;
        valueExpression = null;
        type = ValueType.StringValue;
    }
    public ValueExpr(JId numeric) {
        numericValue = new IdExpr(numeric,false,false);
        stringValue = null;
        sb = null;
        valueExpression = null;
        type = ValueType.NumericValue;
    }
    public ValueExpr(int numeric) {
        numericValue = new IdExpr(numeric,false,false);
        stringValue = null;
        sb = null;
        valueExpression = null;
        type = ValueType.NumericValue;
    }
    public ValueExpr(ExpressionListExpr<? super QExpression> listOfValues) {
        valueExpression = listOfValues;
        stringValue = null;
        numericValue = null;
        sb = null;
        type = ValueType.ListValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValueExpr valueExpr = (ValueExpr) o;

        if (stringValue != null ? !stringValue.equals(valueExpr.stringValue) : valueExpr.stringValue != null)
            return false;
        return numericValue != null ? numericValue.equals(valueExpr.numericValue) : valueExpr.numericValue == null;
    }

    @Override
    public int hashCode() {
        int result = stringValue != null ? stringValue.hashCode() : 0;
        result = 31 * result + (numericValue != null ? numericValue.hashCode() : 0);
        return result;
    }

    void sumIfNumeric(ValueExpr expr) {
        if (numericValue != null && expr.numericValue != null) {
            numericValue.key.add(expr.numericValue.key);
        }
    }

    void mulIfNumeric(ValueExpr expr) {
        if (numericValue != null && expr.numericValue != null) {
            numericValue.key.multiply(expr.numericValue.key);
        }
    }

    void openStringBuilder() {
        sb = new StringBuilder();
    }

    void appendIfString(ValueExpr expr) {
        if (stringValue != null && expr.stringValue != null) {
            sb.append(expr.stringValue);
        }
    }

    void closeStringBuilder() {
        stringValue = sb.toString();
        sb = null;
    }

    @Override
    public int compareTo(QExpression qExpression) {
        int cmp = qExpression._case().compareTo(Type.Value);
        if (cmp == 0) {
            ValueExpr c = (ValueExpr)qExpression;
            if (stringValue != null && c.stringValue != null)
                return stringValue.compareTo(c.stringValue);
            else if (numericValue != null && c.numericValue != null)
                return numericValue.compareTo(c.numericValue);
            else if (stringValue != null) {
                return -1;
            } else {
                return 1;
            }
        } else return cmp;
    }

    @Override
    public String toString() {
        switch (type) {
            case StringValue:
                return stringValue;
            case NumericValue:
                return numericValue.toString();
            case ListValue:
                return valueExpression.toString();
        }
        return "null";
    }
}
