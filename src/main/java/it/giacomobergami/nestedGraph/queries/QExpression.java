package it.giacomobergami.nestedGraph.queries;


import it.giacomobergami.nestedGraph.utils.JId;

public interface QExpression extends Comparable<QExpression> {
    JId numeric();
    enum Type {
        Aggregation,
        Empty,
        List,
        FieldGet,
        FieldSet,
        Id,
        Value
    }
    Type _case();
}
