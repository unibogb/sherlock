package it.giacomobergami.nestedGraph.queries;


import it.giacomobergami.nestedGraph.utils.JId;

import java.util.*;

public class ExpressionListExpr<T extends QExpression> implements List<T>, QExpression {
    List<T> elements;

    @Override
    public JId numeric() {
        return JId.ZERO;
    }

    @Override
    public QExpression.Type _case() {
        return Type.List;
    }


    enum ListType {
        ArrayList,
        LinkedList
    }

    public ExpressionListExpr() {
        this(ListType.ArrayList);
    }

    public ExpressionListExpr(ListType t) {
        switch (t) {
            case LinkedList:
                elements = new LinkedList<>();
                break;
            default:
                elements = new ArrayList<>();
        }
    }

    public ExpressionListExpr(T... elems) {
        elements = new ArrayList<>(elems.length);
        for (int i = 0; i<elems.length; i++) {
            elements.add(elems[i]);
        }
    }

    public static ExpressionListExpr ele = new ExpressionListExpr();

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return elements.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return elements.iterator();
    }

    @Override
    public Object[] toArray() {
        return elements.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return elements.toArray(ts);
    }

    @Override
    public boolean add(T expression) {
        return elements.add(expression);
    }

    @Override
    public boolean remove(Object o) {
        return elements.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return elements.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        return elements.addAll(collection);
    }

    @Override
    public boolean addAll(int i, Collection<? extends T> collection) {
        return elements.addAll(i, collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return elements.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return elements.retainAll(collection);
    }

    @Override
    public void clear() {
        elements.clear();
    }

    @Override
    public T get(int i) {
        return elements.get(i);
    }

    @Override
    public T set(int i, T expression) {
        return elements.set(i,expression);
    }

    @Override
    public void add(int i, T expression) {
        elements.add(i,expression);
    }

    @Override
    public T remove(int i) {
        return elements.remove(i);
    }

    @Override
    public int indexOf(Object o) {
        return elements.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return elements.lastIndexOf(o);
    }

    @Override
    public ListIterator<T> listIterator() {
        return elements.listIterator();
    }

    @Override
    public ListIterator<T> listIterator(int i) {
        return elements.listIterator(i);
    }

    @Override
    public List<T> subList(int i, int i1) {
        return elements.subList(i, i1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExpressionListExpr QExpressions = (ExpressionListExpr) o;

        return elements.equals(QExpressions.elements);
    }

    @Override
    public int hashCode() {
        return elements.hashCode();
    }

    @Override
    public int compareTo(QExpression qExpression) {
        int cmp = qExpression._case().compareTo(Type.List);
        if (cmp == 0) {
            ExpressionListExpr rhe = (ExpressionListExpr)qExpression;
            Iterator<T> lIt = iterator();
            Iterator rIt = rhe.iterator();
            while (lIt.hasNext() && rIt.hasNext()) {
                T left = lIt.next();
                QExpression right = (QExpression)rIt.next();
                cmp = left.compareTo(right);
                if (cmp != 0) return cmp;
            }
            if (lIt.hasNext()) return 1;
            if (rIt.hasNext()) return -1;
            return 0;
        } else return cmp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        elements.forEach(sb::append);
        sb.append(']');
        return sb.toString();
    }
}
