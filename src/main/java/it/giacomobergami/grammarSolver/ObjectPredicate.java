package it.giacomobergami.grammarSolver;

import it.giacomobergami.nestedGraph.repository.NestedGraph;

import java.util.function.Predicate;

public abstract class ObjectPredicate implements Predicate<NestedGraph.Reference> {
    public final boolean isSingleInstancePredicate;
    protected ObjectPredicate(boolean single) {
        this.isSingleInstancePredicate = single;
    }

    public static ObjectPredicate create(Predicate<NestedGraph.Reference> prop, boolean isSingleInstance) {
        return new ObjectPredicate(isSingleInstance) {
            @Override
            public boolean test(NestedGraph.Reference reference) {
                return prop.test(reference);
            }
        };
    }

    public static ObjectPredicate defaultPredicate = new ObjectPredicate(false) {
        @Override
        public boolean test(NestedGraph.Reference reference) {
            return true;
        }
    };
}
