package it.giacomobergami.grammarSolver;

import it.giacomobergami.nestedGraph.HVertex;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.queries.Value;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.nestedGraph.repository.TransformNGraph;
import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.operators.UnaryOperator;
import it.giacomobergami.utils.FilterIterator;
import it.giacomobergami.utils.MapIterator;
import it.giacomobergami.utils.states.TransformingState;

import java.util.*;

public class GrammarSolver implements UnaryOperator{

    JId initRule;
    MatchingQuery matcher;
    List<JPair<PatternNGraph, TransformNGraph>> grammar;
    Set<NestedGraph.Reference> toRemoveVertices;
    Set<NestedGraph.Reference> toRemoveEdges;
    Map<JId, HVertex> vertexTransformation, edgeTransformation;
    NestedGraph result;
    private boolean debug;

    public GrammarSolver(JId initRule) {
        this.initRule = initRule;
        matcher = new MatchingQuery();
        toRemoveVertices = new HashSet<>();
        toRemoveEdges = new HashSet<>();
        grammar = new ArrayList<>();
        vertexTransformation = new HashMap<>();
        edgeTransformation = new HashMap<>();
    }

    private void clear() {
        toRemoveEdges.clear();
        toRemoveVertices.clear();
        result = new NestedGraph();
        vertexTransformation.clear();
        edgeTransformation.clear();
    }

    public GrammarSolver add(PatternNGraph head, TransformNGraph tail) {
        grammar.add(new JPair<>(head, tail));
        return this;
    }

    private Iterator<JId> evaluateVerticesIdForEdges(IdExpr current, JId ruleId, PatternNGraph head,
                                                     TransformingState morphism, JId morphismId) {
        //Iterator<JPair<IdExpr, ObjectPredicate>> it = tail.iterateOverVertexPredicates();
        //ArrayList<Iterator<JId>> alit = new ArrayList<>();
        /*while (it.hasNext()) {*/
            //JPair<IdExpr, ObjectPredicate> current = it.next();
            if (head.contains(current)) {
                return new MapIterator<NestedGraph.Reference, JId>(morphism.getIterator(current)) {
                    @Override
                    public JId apply(NestedGraph.Reference reference) {
                        return reference.id;
                    }
                };
            } else {
                JId newVertexId = getDovetailVertex(ruleId, morphismId, current);
                return  Collections.singletonList(newVertexId).iterator();
            }
        //}
        //return new IteratorIterator<>(alit);
    }

    private void evaluateToRemoveVerticesWithGeneration(JId ruleId, PatternNGraph head, TransformNGraph tail,
                                                        Collection<TransformingState> morphisms, NestedGraph dataGraph) {
        // Counting the matches
        int morphismCount = 0;
        // The result of all the matches
        for (TransformingState morphism : morphisms) {
            // Return all the vertices targetElement involved in the current tail
            Iterator<JPair<IdExpr, ObjectPredicate>> headIterator = head.iterateOverVertexPredicates();
            while (headIterator.hasNext()) {
                JPair<IdExpr, ObjectPredicate> headVertex = headIterator.next();

                // Return all the elements
                Iterator<NestedGraph.Reference> fromMorphism = morphism.getIterator(headVertex.key);
                NestedGraph.Reference isMatchedInTail = tail.getRawVertex(headVertex.key.key);

                // If the morphism represents a vertex
                 // For all the others, I have to detect which vertices have to bee removed, and which haven't
                while (fromMorphism.hasNext()) {
                    NestedGraph.Reference x = fromMorphism.next();
                    if (isMatchedInTail != null) {
                        // I do not have to remove the elements that have been matched uniquely
                        toRemoveVertices.remove(x);
                        if (isMatchedInTail.solve().isTransformation()) {
                            vertexTransformation.put(
                                    x.id,
                                    new HVertex()
                                        .copyFrom(x.solve())
                                            .copyFrom(isMatchedInTail.solve())
                                            .updateWithExternal(morphism, dataGraph));
                        }
                    } else {
                        // I have to remove those vertices that do not appear in a single matched state
                        toRemoveVertices.add(x);
                    }
                }
            }
            Iterator<JPair<IdExpr, ObjectPredicate>> toAggregateIterator = tail.iterateOverVertexPredicates();
            while (toAggregateIterator.hasNext()) {
                JPair<IdExpr, ObjectPredicate> current = toAggregateIterator.next();
                JId newVertexId = getDovetailVertex(ruleId, morphismCount, current.key);
                if (head.getRawVertex(current.key.key) == null) {
                    result.insertNewVertexWithForcedIndex(newVertexId)
                            .setAndUpdate(tail.getRawVertex(current.key.key).solve(), morphism, dataGraph);
                }
            }
            morphismCount++;
        }
    }

    private static JId getDovetailVertex(JId ruleId, int morphismCount, IdExpr tailElement) {
        return JId.dovetail(JId.ONE, JId.dovetail(ruleId, JId.generateFromList(new JId(morphismCount), tailElement.key)));
    }
    private static JId getDovetailVertex(JId ruleId, JId morphismCount, IdExpr tailElement) {
        return JId.dovetail(JId.ONE, JId.dovetail(ruleId, JId.generateFromList(new JId(morphismCount), tailElement.key)));
    }
    private static JId getDovetailEdge(JId ruleId, int morphismCount, IdExpr tailElement, JId src, JId dst) {
        return JId.dovetail(JId.TWO, JId.dovetail(ruleId, JId.generateFromList(new JId(morphismCount), tailElement.key, src, dst)));
    }
    private static JId getDovetailEdge(JId ruleId, JId morphismCount, IdExpr tailElement, JId src, JId dst) {
        return JId.dovetail(JId.TWO, JId.dovetail(ruleId, JId.generateFromList(morphismCount, tailElement.key, src, dst)));
    }

    private void evaluateToRemoveEdgesWithGeneration(JId ruleId, PatternNGraph head, TransformNGraph tail,
                                                     Collection<TransformingState> morphisms, NestedGraph dataGraph) {
        int morphismCount = 0;
        // The result of all the matches
        for (TransformingState morphism : morphisms) {
            // Return all the vertices targetElement involved in the current tail
            Iterator<JPair<IdExpr, ObjectPredicate>> headIterator = head.iterateOverEdgePredicates();
            while (headIterator.hasNext()) {
                JPair<IdExpr, ObjectPredicate> headVertex = headIterator.next();

                // Return all the elements
                Iterator<NestedGraph.Reference> fromMorphism = morphism.getIterator(headVertex.key);
                NestedGraph.Reference isMatchedInTail = tail.getRawEdge(headVertex.key.key);

                // If the morphism represents a vertex
                // For all the others, I have to detect which vertices have to bee removed, and which haven't
                while (fromMorphism.hasNext()) {
                    NestedGraph.Reference x = fromMorphism.next();
                    if (isMatchedInTail != null) {
                        // I do not have to remove the elements that have been matched uniquely
                        toRemoveEdges.remove(x);
                        if (isMatchedInTail.solve().isTransformation()) {
                            edgeTransformation.put(
                                    x.id,
                                    new HVertex().
                                            copyFrom(x.solve()).
                                            copyFrom(isMatchedInTail.solve()).
                                            updateWithExternal(morphism, dataGraph));
                        }
                    } else {
                        // I have to remove those vertices that do not appear in a single matched state
                        toRemoveEdges.add(x);
                    }
                }
            }
            /// TODO: this.gva.generateAggregation(ruleId, head, tail, dataGraph, morphismCount, morphism);
            // TODO: Aggregated vertices are already put in the result graph
            Iterator<JPair<IdExpr, ObjectPredicate>> toAggregateIterator = tail.iterateOverEdgePredicates();
            while (toAggregateIterator.hasNext()) {
                JPair<IdExpr, ObjectPredicate> current = toAggregateIterator.next();

                ////JId newVertexId = getDovetailVertex(ruleId, morphismCount, current.key);
                if (head.getRawEdge(current.key.key) == null) {
                    NestedGraph.Reference isSingleMatchElement = tail.getRawEdge(current.key.key);
                    JId mId = new JId(morphismCount);
                    Iterable<JId> spC = () ->
                    evaluateVerticesIdForEdges(isSingleMatchElement.src().asIdExpr(), ruleId, head, morphism, mId);
                    Iterable<JId> tpC = () ->
                    evaluateVerticesIdForEdges(isSingleMatchElement.dst().asIdExpr(), ruleId, head, morphism, mId);
                    for (JId sp : spC) {
                        for (JId tp : tpC) {
                            JId newEdgeId = getDovetailEdge(ruleId, morphismCount, current.key, sp, tp);
                            result.insertNewEdgeWithForcedIndex(newEdgeId, sp, tp)
                                    .setAndUpdate(isSingleMatchElement.solve(), morphism, dataGraph);
                        }
                    }
                }
            }

            morphismCount++;
        }
    }
    /**
     * I must remove those elements that have been matched only within an aggregated state.
     * An aggregated state is a state not appearing in the head, but only on the tail
     * @param head
     * @param data
     * @return
     */
    private Collection<TransformingState> toRemove(PatternNGraph head, NestedGraph data) {
        return matcher.patternMatching(head, data);
    }

    @Override
    public NestedGraph apply(NestedGraph data) {
        clear();
        // Setting all the vertices and edges that have to be removed
        JId ruleId = initRule.add(0);
        ArrayList<TransformingState> morphisms = new ArrayList<>();
        for (JPair<PatternNGraph, TransformNGraph> rule : grammar) {
            Collection<TransformingState> matchesResult = matcher.patternMatching(rule.key, data);
            if (debug) {
                System.out.println("MATCHES: ");
                matchesResult.forEach(System.out::println);
            }
            evaluateToRemoveVerticesWithGeneration(ruleId, rule.key, rule.value, matchesResult, data);
            evaluateToRemoveEdgesWithGeneration(ruleId, rule.key, rule.value, matchesResult, data);
            ruleId = ruleId.add(1);
            morphisms.addAll(matchesResult);
        }
        retainOldVertices(data);
        retainOldEdges(data);
        return result;
    }

    // Utility methods
    private FilterIterator<JPair<IdExpr, NestedGraph.Reference>> removeElements
    (Iterator<JPair<IdExpr, NestedGraph.Reference>> it, Set<NestedGraph.Reference> removalSet) {
        // Removing all the vertices that have to be removed
        return new FilterIterator<JPair<IdExpr, NestedGraph.Reference>>(it) {
            @Override
            public boolean test(JPair<IdExpr, NestedGraph.Reference> idExprReferenceJPair) {
                return !removalSet.contains(idExprReferenceJPair.value);
            }
        };
    }

    private void retainOldVertices(NestedGraph data) {
        removeElements(data.iterateOverVertices(), toRemoveVertices)
                .forEachRemaining(current -> {

                    HVertex newElement = result.insertNewVertexWithForcedIndex(current.key.key).solve();
                    newElement.copyFrom(vertexTransformation.getOrDefault(current.key.key,current.value.solve()));
                        }
                );
    }

    private void retainOldEdges(NestedGraph data) {
        // PREPROCESSING
        Set<NestedGraph.Reference> toRetain = new HashSet<>();
        for (NestedGraph.Reference edgeReference : toRemoveEdges) {
            NestedGraph.Reference src = edgeReference.src();
            NestedGraph.Reference dst = edgeReference.dst();

            if (hasMatch(src, toRemoveVertices, result) ||
                    hasMatch(dst, toRemoveVertices, result)) {
                toRetain.add(edgeReference);
            }
        }
        toRemoveEdges.removeAll(toRetain);

        // ELEMENTS
        removeElements(data.iterateOverEdges(), toRemoveEdges)
                .forEachRemaining(current -> {
                    HVertex element = result
                            .insertNewEdgeWithForcedIndex(current.key.key, current.value.src().id, current.value.dst().id)
                            .solve();
                    element.copyFrom(edgeTransformation.getOrDefault(current.key.key,current.value.solve()));
                });
    }

    private static boolean hasMatch(NestedGraph.Reference src, Set<NestedGraph.Reference> toRemoveVertices,
                                    NestedGraph result) {
        if (!toRemoveVertices.contains(src)) {
            return true;
        } else {
            Iterator<JPair<IdExpr, NestedGraph.Reference>> it = result.iterateOverVertices();
            while (it.hasNext()) {
                HVertex current = it.next().value.solve();
                for (String label : current.getLabels()) {
                    Value property = current.get(label);
                    switch (property.key._case()) {
                        case Id:
                        case Value:{
                            if (property.value.contains(src.asIdExpr())) return true;
                        }
                        break;
                        default:
                            //noop
                    }
                }
            }
        }
        return false;
    }

    public GrammarSolver add(JPair<PatternNGraph, TransformNGraph> moveDeterminerPattern) {
        grammar.add(moveDeterminerPattern);
        return this;
    }

    public void setDebug() {
        debug = true;
    }
}
