package it.giacomobergami.grammarSolver;

import com.google.common.collect.HashMultimap;
import com.sun.istack.internal.Nullable;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.utils.registers.MorphismRegister;
import it.giacomobergami.utils.states.MatchingState;
import it.giacomobergami.utils.states.TransformingState;

import java.util.*;

public class MatchingQuery {

    HashMultimap<IdExpr, NestedGraph.Reference> multimapForPrecomputedMorphism;
    NestedGraph data;
    PatternNGraph query;
    ArrayList<TransformingState> restituire;

    public MatchingQuery() {
        restituire = new ArrayList<>();
        multimapForPrecomputedMorphism = HashMultimap.create();
        data = null;
    }

    /**
     * Isomorphism Matching
     * @param gen               Property Id from which start the edge visit
     * @param prop              Property for the edges
     * @param restituire
     * @return
     */
    private void patternMatching(NestedGraph.Reference srcVertex, IdExpr gen, NestedGraph.Reference prop, @Nullable MatchingState status, ArrayList<TransformingState> restituire) {
        Iterator<NestedGraph.Reference> vertexIterator = multimapForPrecomputedMorphism.get(gen).iterator();
        while (vertexIterator.hasNext()) {
            NestedGraph.Reference w = vertexIterator.next();
            if (srcVertex == null || w.src().equals(srcVertex))
                fillWith(w, gen, prop, status == null ? new MatchingState() : status.doBranch(), restituire);
        }
    }

    private void graphTraversalOverDataVertices(IdExpr gen, NestedGraph.Reference prop, @Nullable MatchingState status, Collection<TransformingState> restituire) {
        Iterator<JPair<IdExpr, NestedGraph.Reference>> itDe = data.iterateOverVertices();
        ArrayList<TransformingState> toAggregate = new ArrayList<>();
        while (itDe.hasNext()) {
            JPair<IdExpr, NestedGraph.Reference> nx2 = itDe.next();
            if (prop.solve().getPredicate().test(nx2.value)) {
                fillWith(nx2.value, gen, prop, status == null ? new MatchingState() : status.doBranch(), toAggregate);
            }
        }
        MorphismRegister.MorphismRegisterMerger.merger.doMerge(toAggregate, query).forEach(x -> addOnCheck(restituire, x));
    }

    private void graphTraversalOverEdges(NestedGraph.Reference object, IdExpr gen, NestedGraph.Reference prop, @Nullable MatchingState status, Collection<TransformingState> restituire) {
        // Now I have to return all the possible paths for this given edge predicate
        Iterator<NestedGraph.Reference> itDe = object.outEdges();
        final ObjectPredicate dstPredicate = prop.dst().solve().getPredicate();
        ArrayList<TransformingState> toAggregate = new ArrayList<>();
        //IdExpr dvie = new IdExpr((int) prop.dst().id, true, true);

        // Among all the possible data paths
        while (itDe.hasNext()) {
            NestedGraph.Reference dataEdge = itDe.next();
            // I select those that satisfy both the current edge predicate and the target its target predicate
            if (prop.solve().getPredicate().test(dataEdge) && dstPredicate.test(dataEdge.dst())) {
                fillWith(dataEdge, gen, prop, status == null ? new MatchingState() : status.doBranch(), toAggregate);
            }
        }
        MorphismRegister.MorphismRegisterMerger.merger.doMerge(toAggregate, query).forEach(x -> addOnCheck(restituire, x));
    }

    private Collection<TransformingState> getSolutions() {
        restituire.clear();
        if (query.vertexCount() > 0) {
            IdExpr gen = new IdExpr(0, true, true);
            NestedGraph.Reference prop = query.getRawVertex(JId.ZERO);
            if (prop.solve().getPredicate().isSingleInstancePredicate) {
                // Starts the isomorphism matching
                patternMatching(null, gen, prop, null, restituire);
            } else {
                graphTraversalOverDataVertices(gen, prop, null, restituire);
            }
        }
        return restituire;
    }




    private void fillWith(final NestedGraph.Reference object,
                          IdExpr predicateId,
                          NestedGraph.Reference predicate,
                          MatchingState status,
                          ArrayList<TransformingState> restituire) {


        // Setting the element as visited, and add it to the result
        if (predicate.solve().getPredicate().isSingleInstancePredicate) {
            if (!status.setMorphismRegisters(predicateId, object))
                return;
        } else {
            status.setMultiMorphismRegisters(predicateId, object);
        }


        if (!status.setVisitedObject(predicate)) {
            // If all the elements by now have been visited, then skip to the nest elements to be visited
            restartToVisitFromRemainingVertices(status, restituire);
        } else {
            // Both the data and the predicate belong to vertices
            // => continue to scan the edges
            if (object.isVertex && predicate.isVertex) {
                Iterator<JPair<IdExpr, NestedGraph.Reference>> itEdgesFromquery = query.iterateOverEdges();

                // If there are no more edges to traverse, just add the final result
                if (!itEdgesFromquery.hasNext()) {
                    addOnCheck(restituire, status);
                } else {
                    boolean hasMet = false;
                    while (itEdgesFromquery.hasNext()) {
                        JPair<IdExpr, NestedGraph.Reference> nx = itEdgesFromquery.next();
                        if (predicateId.key.equals(nx.value.src().id)) {
                            hasMet = true;
                            ObjectPredicate eprop = nx.value.solve().getPredicate();
                            if (eprop.isSingleInstancePredicate) {
                                patternMatching(object, nx.key, nx.value, status, restituire);
                            } else {
                                graphTraversalOverEdges(object, nx.key, nx.value, status, restituire);
                            }
                        }
                    }
                    if (!hasMet) {
                        restartToVisitFromRemainingVertices(status, restituire);
                    }
                }
            } else {
                // I'm just a single edge. Consequently, I must belong to a group-by and I do not have to fear to scan all the elements (1)
                visitingGroupedEdge(object, predicate, status, restituire);
            }
        }
    }

    private void addOnCheck(Collection<TransformingState> restituire, TransformingState state) {
        if (query.testJoinConditions(state))
            restituire.add(state);
    }

    private void addOnCheck(Collection<TransformingState> restituire, MatchingState status) {
        addOnCheck(restituire, status.asTransformingState(query));
    }

    private void restartToVisitFromRemainingVertices(MatchingState status, ArrayList<TransformingState> restituire) {

        Set<IdExpr> visitedPredicates = status.returnSettedRegisters();
        // Start to setVisitedObject from the vertices: check a predicate that has not been visited yet

        // Checks if this is a final path, where all the configurations have been visited, and no errors happened
        boolean hasMetEnd = true;
        for (int i = 0; i < query.vertexCount(); i++) {
            IdExpr gen = new IdExpr(i, true, true);
            if (!(visitedPredicates.contains(gen))) {
                NestedGraph.Reference prop = query.getRawVertex(new JId(i));
                if (prop.solve().getPredicate().isSingleInstancePredicate) {
                    hasMetEnd = false;
                } else {
                    hasMetEnd = false;
                    graphTraversalOverDataVertices(gen, prop, status, restituire);
                }
            }
        }
        if (hasMetEnd) {
            addOnCheck(restituire, status);
        }
    }

    private void visitingGroupedEdge(NestedGraph.Reference object, NestedGraph.Reference predicate, MatchingState status, ArrayList<TransformingState> restituire) {

        NestedGraph.Reference dataDestinationVertex = object.dst();
        NestedGraph.Reference objectWithPredicate2 = predicate.dst();
        IdExpr owpId2 = new IdExpr(objectWithPredicate2.id, true, true);

        // If either I have just one single possibility for the match and this has been met, or this has not been met yet
        if (!predicate.solve().getPredicate().isSingleInstancePredicate ||
                (!status.doMorphismRegistersContainValueInKey(owpId2, dataDestinationVertex) &&
                 !status.doMorphismRegistersContainValue(dataDestinationVertex))) {
            status.setVisitedObject(object);
            ////System.out.println("Edge Traversing...");
            fillWith(dataDestinationVertex, owpId2, objectWithPredicate2, status, restituire);
        }
    }

    /**
     * Performs the pattern matching semantics by using the all or not all semantics
     *
     * @param query Query containing the patterns
     * @param data  Data To be matched
     * @return A list of associations of
     * * key = morphisms (by group-by, unique instances)
     * * value = multiple instances (not fixed pattern)
     */
    public Collection<TransformingState> patternMatching(PatternNGraph query, NestedGraph data) {
        if (multimapForPrecomputedMorphism != null) {
            multimapForPrecomputedMorphism.clear();
        }

        // Vertex Initialization with morphisms
        // Evaluating the starting tables
        multimapForPrecomputedMorphism = HashMultimap.create(query.vertexCount(), data.vertexCount());

        {
            Iterator<JPair<IdExpr, ObjectPredicate>> vIt = query.iterateOverVertexPredicates();
            while (vIt.hasNext()) {
                JPair<IdExpr, ObjectPredicate> cp = vIt.next();
                if (cp.value.isSingleInstancePredicate) {
                    Iterable<JPair<IdExpr, NestedGraph.Reference>> dataV = data::iterateOverVertices;
                    for (JPair<IdExpr, NestedGraph.Reference> d_cp : dataV) {
                        if (cp.value.test(d_cp.value)) {
                            multimapForPrecomputedMorphism.put(cp.key, d_cp.value);
                        }
                    }
                }
            }
        }
        {
            Iterator<JPair<IdExpr, ObjectPredicate>> vIt = query.iterateOverEdgePredicates();
            while (vIt.hasNext()) {
                JPair<IdExpr, ObjectPredicate> cp = vIt.next();
                if (cp.value.isSingleInstancePredicate) {
                    Iterable<JPair<IdExpr, NestedGraph.Reference>> dataV = data::iterateOverEdges;
                    for (JPair<IdExpr, NestedGraph.Reference> d_cp : dataV) {
                        if (cp.value.test(d_cp.value)) {
                            multimapForPrecomputedMorphism.put(cp.key, d_cp.value);
                        }
                    }
                }
            }
        }
        this.data = data;
        this.query = query;
        return getSolutions();
    }


}
