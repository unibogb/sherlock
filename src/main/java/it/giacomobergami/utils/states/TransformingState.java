package it.giacomobergami.utils.states;

import com.google.common.collect.Iterators;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.Gamma;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.utils.IteratorIterator;
import it.giacomobergami.utils.VoidIterator;
import it.giacomobergami.utils.registers.MorphismRegister;
import it.giacomobergami.utils.registers.MultiMorphismRegister;

import java.util.*;

public class TransformingState implements Gamma {
    final MorphismRegister morphism;
    final MultiMorphismRegister multism;
    private final PatternNGraph searchPattern;


    @Override
    public String toString() {
        return morphism.toString() + " " + multism.toString();
    }

    // Observe: the state can be only get from the elements

    /**
     *
     * @param morphism          Registers have been set for unique elements. Contains references to the data graph
     * @param multism           Registers have been set for multiple elements. Contains references to the data graph
     * @param searchPattern     Contains elements that exists in the data graph
     */
    public TransformingState(MorphismRegister morphism, MultiMorphismRegister multism, PatternNGraph searchPattern) {
        this.morphism = morphism;
        this.multism = multism;
        this.searchPattern = searchPattern;
    }


    public Iterator<NestedGraph.Reference> resolveMatchedObjects(IdExpr element) {
        return getIterator(element);
    }

    /**
     * Returns the object that have been matched either on the morphism, or on the multi-morphism, only if the id corresponds
     * to the id-s within the search pattern.
     *
     * @param element       Transform Graph element's id to be transformed into actual graph vertices
     * @return              Resolved elements
     */
    @Override
    public Iterator<NestedGraph.Reference> getIterator(IdExpr element) {
        return searchPattern.contains(element) ?
                Iterators.concat(morphism.getRegister(element), multism.getRegister(element))
                : new VoidIterator<>();
    }

    @Override
    public void instantiateFromIterator(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns) {
        if (searchPattern.contains(id)) {
            morphism.fillFromRegisterOf(id, resolveIdsWithPatterns);
            multism.fillFromRegisterOf(id, resolveIdsWithPatterns);
        }
    }

    public boolean hasMatchWith(NestedGraph.Reference dst) {
        return morphism.containsValue(dst) || multism.returnValues().contains(dst);
    }

    public Collection<NestedGraph.Reference> returnValues() {
        Collection<NestedGraph.Reference> valuesInRegisters = morphism.returnValues();
        valuesInRegisters.addAll(multism.returnValues());
        return valuesInRegisters;
    }

    public void getVertexAndEdgeReferences(Collection<NestedGraph.Reference> vRef, Collection<NestedGraph.Reference> eRef) {
        morphism.getVertexAndEdgeReferences(vRef, eRef);
        multism.getVertexAndEdgeReferences(vRef,eRef);
    }

    public void mergeMultismWith(MultiMorphismRegister mergedMultimorphisms) {

        mergedMultimorphisms.mergeWith(multism);
    }

    public void addMorphismToMapList(ArrayList<Map<IdExpr, NestedGraph.Reference>> mapMorphisms) {
        morphism.addToMapList(mapMorphisms);
    }

    public String detailedString() {
        return "<" + morphism.detailedString() + "> && <" + multism.detailedString() + ">";
    }

    public IteratorIterator<IdExpr> returnMorphismsKey() {
        ArrayList<Iterator<IdExpr>> alw = new ArrayList<>(2);
        alw.add(morphism.returnSettedRegisters().iterator());
        alw.add(multism.returnSettedRegisters().iterator());
        return new IteratorIterator<>(alw);
    }
}
