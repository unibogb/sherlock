package it.giacomobergami.utils.states;

import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.utils.registers.MorphismRegister;
import it.giacomobergami.utils.registers.MultiMorphismRegister;
import it.giacomobergami.utils.registers.RegisterDump;

import java.util.HashSet;
import java.util.Set;

public class MatchingState {
    public MorphismRegister morphism;
    public MultiMorphismRegister multism;
    Set<NestedGraph.Reference> visitedObjects;

    public MatchingState() {
        morphism = new MorphismRegister();
        multism = new MultiMorphismRegister();
        visitedObjects = new HashSet<>();
    }

    public MatchingState(MorphismRegister morphism, MultiMorphismRegister multi, Set<NestedGraph.Reference> ref) {
        this.morphism = morphism;
        this.multism = multi;
        this.visitedObjects = ref;
    }

    public MatchingState doBranch() {
        return new MatchingState(morphism.copy(), multism.copy(), new HashSet<>(visitedObjects));
    }

    public boolean setMorphismRegisters(IdExpr predicateId, NestedGraph.Reference object) {
        return morphism.setRegister(predicateId, object);
    }

    public void setMultiMorphismRegisters(IdExpr predicateId, NestedGraph.Reference object) {
        multism.setRegister(predicateId, object);
    }

    public Set<IdExpr> returnSettedRegisters() {
        Set<IdExpr> toret = morphism.returnSettedRegisters();
        toret.addAll(multism.returnSettedRegisters());
        return toret;
    }

    public boolean setVisitedObject(NestedGraph.Reference predicate) {
        return visitedObjects.add(predicate);
    }

    public RegisterDump dumpRegisters() {
        return new RegisterDump(morphism, multism);
    }

    public boolean doMorphismRegistersContainValue(NestedGraph.Reference dataDestinationVertex) {
        return morphism.containsValue(dataDestinationVertex);
    }

    public boolean doMorphismRegistersContainValueInKey(IdExpr key, NestedGraph.Reference dataDestinationVertex) {
        return morphism.containsRegisterWithValue(key, dataDestinationVertex);
    }

    public TransformingState asTransformingState(PatternNGraph searchPattern) {
        return new TransformingState(morphism, multism, searchPattern);
    }
}
