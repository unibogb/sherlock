package it.giacomobergami.utils.morphismFunctionalList;

import it.giacomobergami.nestedGraph.queries.ExpressionListExpr;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.OptRef;

import java.util.*;
import java.util.stream.Collectors;

public class MLHead implements MorphismList {

    //private Map<NestedGraph.Reference, JPair<IdExpr, Integer>> map = new HashMap<>();
    NestedGraph.Reference key;
    IdExpr valueLeft;
    Integer valueRight;
    int precomputedSize;
    MorphismList rest;

    @Override
    public Map<IdExpr, NestedGraph.Reference> asMap() {
        HashMap<IdExpr, NestedGraph.Reference> toret = new HashMap<>();
        MorphismList cpyTail = this;
        while (cpyTail.size() > 0) {
            MLHead k = ((MLHead) cpyTail);
            toret.put(k.valueLeft, k.key);
            cpyTail = ((MLHead)cpyTail).rest;
        }
        return toret;
    }

    @Override
    public String toString() {
        return key + "{" + valueLeft + " @pos=" + valueRight + "}, " + rest;
    }

    MLHead(NestedGraph.Reference key, IdExpr value, MorphismList tail) {
        int tSize = tail.size();
        /*if (tSize > 0) {
            map.putAll(((HeadMFL)tail).map);
        }*/
        this.key = key;
        this.valueLeft = value;
        valueRight = tSize;
        rest = tail;
        //map.put(key, new JPair<>(value, tSize));
        precomputedSize = tSize + 1;

    }

    @Override
    public int size() {
        return precomputedSize;
    }

    @Override
    public Set<NestedGraph.Reference> morphismReferences() {
        Set<NestedGraph.Reference> references = new HashSet<>();
        MorphismList cpyTail = this;
        while (cpyTail.size() > 0) {
            references.add(((MLHead)cpyTail).key);
            cpyTail = ((MLHead)cpyTail).rest;
        }
        return references;
    }

    @Override
    public void morphismReferences(Collection<NestedGraph.Reference> vertexReference, Collection<NestedGraph.Reference> edgeReference) {
        MorphismList cpyTail = this;
        while (cpyTail.size() > 0) {
            NestedGraph.Reference k = ((MLHead) cpyTail).key;
            if (k.isVertex)
                vertexReference.add(k);
            else
                edgeReference.add(k);
            cpyTail = ((MLHead)cpyTail).rest;
        }
    }

    @Override
    public Set<IdExpr> morphismPredicates() {
        Set<IdExpr> references = new HashSet<>();
        MorphismList cpyTail = this;
        while (cpyTail.size() > 0) {
            references.add(((MLHead)cpyTail).valueLeft);
            cpyTail = ((MLHead)cpyTail).rest;
        }
        return references;
    }

    @Override
    public OptRef<MorphismList> addMorphismWith(NestedGraph.Reference ref, IdExpr queryIdElem) {
        return containsKey(ref) ?
                new OptRef<>() :
                new OptRef<>(new MLHead(ref, queryIdElem, this));
    }

    @Override
    public boolean containsKey(NestedGraph.Reference v) {
        MorphismList cpyTail = this;
        while (cpyTail.size() > 0) {
            if ((((MLHead)cpyTail).key).equals(v)) {
                return true;
            }
            cpyTail = ((MLHead)cpyTail).rest;
        }
        return false;
    }

    @Override
    public boolean containsKey(NestedGraph.Reference v, IdExpr expr) {
        MorphismList cpyTail = this;
        while (cpyTail.size() > 0) {
            if ((((MLHead)cpyTail).key).equals(v) && ((MLHead)cpyTail).valueLeft.equals(expr)) {
                return true;
            }
            cpyTail = ((MLHead)cpyTail).rest;
        }
        return false;
    }

    @Override
    public void morphismPredicates(Set<IdExpr> mks) {
        MorphismList cpyTail = this;
        mks.add(((MLHead)cpyTail).valueLeft);
        cpyTail = ((MLHead)cpyTail).rest;
        if (cpyTail.size() > 0) {
            ((MLHead)cpyTail).morphismPredicates(mks);
        }
    }

    @Override
    public Iterator<NestedGraph.Reference> getMorphismOf(IdExpr elementId) {
        return valueLeft.equals(elementId) ?
                  Arrays.stream(new NestedGraph.Reference[]{key}).iterator()
                : rest.getMorphismOf(elementId);
    }

    @Override
    public void instantiateMorphismOf(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns) {
        MorphismList cpyTail = this;
        if (valueLeft.equals(id)) {
            resolveIdsWithPatterns.add(key.asIdExpr());
        } else {
            cpyTail = ((MLHead)cpyTail).rest;
            if (cpyTail.size() > 0) {
                ((MLHead)cpyTail).instantiateMorphismOf(id,resolveIdsWithPatterns);
            }
        }
    }

    @Override
    public String detailedString() {
        return asMap()
                .entrySet()
                .stream()
                .map(x -> "["+x.getKey()+"]="+x.getValue().solve().toString()+"\t")
                .collect(Collectors.joining(";"));
    }
}
