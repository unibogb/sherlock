package it.giacomobergami.utils.morphismFunctionalList;

import it.giacomobergami.nestedGraph.queries.ExpressionListExpr;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.OptRef;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Defining some registers that can be only set once. When a register is set multiple times, it returns a null object
 *
 * In particular, this class is used to implement the registers for the (iso)morphisms (not "for all")
 */
public interface MorphismList {

    Map<IdExpr, NestedGraph.Reference> asMap();

    int size();
    Set<NestedGraph.Reference> morphismReferences();

    void morphismReferences(Collection<NestedGraph.Reference> vertexReference, Collection<NestedGraph.Reference> edgeReference);

    Set<IdExpr> morphismPredicates();
    OptRef<MorphismList> addMorphismWith(NestedGraph.Reference ref, IdExpr queryIdElem);
    default OptRef<MorphismList> addMorphismWith(NestedGraph.Reference ref) {
        return addMorphismWith(ref, IdExpr.BOGUS);
    }

    static MorphismList create() {
        return new MLEmpty();
    }

    boolean containsKey(NestedGraph.Reference v);
    boolean containsKey(NestedGraph.Reference v, IdExpr expr);
    void morphismPredicates(Set<IdExpr> mks);


    Iterator<NestedGraph.Reference> getMorphismOf(IdExpr elementId);
    void instantiateMorphismOf(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns);

    String detailedString();
}
