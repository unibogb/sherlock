package it.giacomobergami.utils.morphismFunctionalList;

import it.giacomobergami.nestedGraph.queries.ExpressionListExpr;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.OptRef;
import it.giacomobergami.utils.VoidIterator;

import java.util.*;

public class MLEmpty implements MorphismList {

    private final static Set<NestedGraph.Reference> map = new LinkedHashSet<>();
    private final static Set<IdExpr> iem = new LinkedHashSet<>();
    private final static Iterator<NestedGraph.Reference> voidIterator = new VoidIterator<>();

    protected MLEmpty() {}

    @Override
    public Map<IdExpr, NestedGraph.Reference> asMap() {
        return new HashMap<>();
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Set<NestedGraph.Reference> morphismReferences() {
        return map;
    }

    @Override
    public void morphismReferences(Collection<NestedGraph.Reference> vertexReference, Collection<NestedGraph.Reference> edgeReference) {

    }

    @Override
    public Set<IdExpr> morphismPredicates() {
        return iem;
    }

    @Override
    public OptRef<MorphismList> addMorphismWith(NestedGraph.Reference ref, IdExpr queryIdElem) {
        return new OptRef<>(new MLHead(ref, queryIdElem, this));
    }

    @Override
    public boolean containsKey(NestedGraph.Reference v) {
        return false;
    }

    @Override
    public boolean containsKey(NestedGraph.Reference v, IdExpr expr) {
        return false;
    }

    @Override
    public void morphismPredicates(Set<IdExpr> mks) {
        //noop
    }

    @Override
    public Iterator<NestedGraph.Reference> getMorphismOf(IdExpr elementId) {
        return voidIterator;
    }

    @Override
    public void instantiateMorphismOf(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns) {

    }

    @Override
    public String detailedString() {return "";}

    @Override
    public String toString() {return "";}
}
