package it.giacomobergami.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Arrays; /* For sample code */

public class IteratorIterator<T> implements Iterator<T> {
    private final ArrayList<Iterator<T>>  is;
    private int current;

    public IteratorIterator(ArrayList<Iterator<T>> iterators)
    {
            is = iterators;
            current = 0;
    }

    public boolean hasNext() {
            while ( current < is.size() && !is.get(current).hasNext() )
                    current++;

            return current < is.size();
    }

    public T next() {
            while ( current < is.size() && !is.get(current).hasNext() )
                    current++;

            return is.get(current).next();
    }
}