package it.giacomobergami.utils;

import java.util.Iterator;
import java.util.function.Function;

public abstract class MapIterator<S,T> implements Iterator<T>, Function<S,T> {
    private final Iterator<S> sourceIterator;
    public MapIterator(Iterator<S> sourceIterator) {
        this.sourceIterator = sourceIterator;
    }
    @Override
    public boolean hasNext() {
        return sourceIterator.hasNext();
    }
    @Override
    public T next() {
        return apply(sourceIterator.next());
    }
}
