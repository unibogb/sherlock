package it.giacomobergami.utils.registers;

import com.google.common.collect.HashMultimap;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.utils.morphismFunctionalList.MorphismList;

public class RegisterDump extends JPair<MorphismRegister, MultiMorphismRegister>  {
    public RegisterDump(MorphismRegister key, MultiMorphismRegister value) {
        super(key, value);
    }
}
