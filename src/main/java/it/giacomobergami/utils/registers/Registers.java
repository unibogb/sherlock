package it.giacomobergami.utils.registers;

import it.giacomobergami.nestedGraph.queries.ExpressionListExpr;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public interface Registers {
    boolean setRegister(IdExpr predicateId,NestedGraph.Reference object);
    boolean isValidState();
    Set<IdExpr> returnSettedRegisters();
    <T extends Registers> T copy();

    Collection<NestedGraph.Reference> returnValues();

    Iterator<NestedGraph.Reference> getRegister(IdExpr elementId);
    void fillFromRegisterOf(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns);
    void getVertexAndEdgeReferences(Collection<NestedGraph.Reference> vRef, Collection<NestedGraph.Reference> eRef);
    String detailedString();
}
