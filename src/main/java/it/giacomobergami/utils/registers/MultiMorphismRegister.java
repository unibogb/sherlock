package it.giacomobergami.utils.registers;

import com.google.common.collect.HashMultimap;
import it.giacomobergami.nestedGraph.queries.ExpressionListExpr;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.utils.VoidIterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

public class MultiMorphismRegister implements Registers {

    HashMultimap<IdExpr, NestedGraph.Reference> state;

    public MultiMorphismRegister() {
        state = HashMultimap.create();
    }

    public void mergeWith(MultiMorphismRegister right) {
        state.putAll(right.state);
    }

    private MultiMorphismRegister(HashMultimap<IdExpr, NestedGraph.Reference> previousState) {
        state = HashMultimap.create(previousState);
    }

    @Override
    public boolean setRegister(IdExpr predicateId, NestedGraph.Reference object) {
        state.put(predicateId, object);
        return true;
    }

    @Override
    public boolean isValidState() {
        return true;
    }

    @Override
    public Set<IdExpr> returnSettedRegisters() {
        return state.keySet();
    }

    @Override
    public Collection<NestedGraph.Reference> returnValues() {
        return state.values();
    }

    @Override
    public Iterator<NestedGraph.Reference> getRegister(IdExpr elementId) {
        Set<NestedGraph.Reference> v = state.get(elementId);
        return v != null ? v.iterator() : new VoidIterator<>();
    }

    @Override
    public void fillFromRegisterOf(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns) {
        Set<NestedGraph.Reference> v = state.get(id);
        if (v != null) for (NestedGraph.Reference x : v) resolveIdsWithPatterns.add(x.asIdExpr());
    }

    @Override
    public void getVertexAndEdgeReferences(Collection<NestedGraph.Reference> vRef, Collection<NestedGraph.Reference> eRef) {
        for (NestedGraph.Reference x : state.values()) {
            if (x.isVertex)
                vRef.add(x);
            else
                eRef.add(x);
        }
    }

    @Override
    public String detailedString() {
        return state.asMap().entrySet()
                .stream()
                .map(x -> "["+x.getKey()+"]={"+x.getValue().stream().map(y -> y.solve().toString()).collect(Collectors.joining(","))+"}\t")
                .collect(Collectors.joining(";"));
    }

    @Override
    public MultiMorphismRegister copy() {
        return new MultiMorphismRegister(state);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MultiMorphismRegister)) return false;

        MultiMorphismRegister that = (MultiMorphismRegister) o;

        return state.equals(that.state);
    }

    @Override
    public int hashCode() {
        return state.hashCode();
    }

    @Override
    public String toString() {
        return state.toString();
    }
}
