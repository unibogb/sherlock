package it.giacomobergami.utils.registers;

import com.google.common.collect.HashMultimap;
import it.giacomobergami.nestedGraph.queries.IdExpr;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.nestedGraph.utils.OptRef;
import it.giacomobergami.utils.VoidIterator;
import it.giacomobergami.utils.morphismFunctionalList.MorphismList;
import it.giacomobergami.utils.states.TransformingState;

import java.util.*;

public class MorphismRegister implements Registers {

    OptRef<MorphismList> state;

    public void addToMapList(ArrayList<Map<IdExpr, NestedGraph.Reference>> mapMorphisms) {
        mapMorphisms.add(state.unsafeGet().asMap());
    }

    /**
     * When traversing an forall path, allows to check which paths can be actually merged.
     */
    public static class MorphismRegisterMerger {
        ArrayList<Map<IdExpr, NestedGraph.Reference>> mapMorphisms;
        IdExpr[] settedRegisters;
        List<TransformingState> toMergeAtTheEnd;
        Set<TransformingState> mergedStates;
        HashSet<Integer> indices;
        PatternNGraph toSubst;

        public final static MorphismRegisterMerger merger = new MorphismRegisterMerger();
        private MorphismRegisterMerger() {}

        private void merge(int i, Collection<Integer> indices) {
            if (i >= settedRegisters.length) {
                if (indices.size() == 1) {
                    mergedStates.add(toMergeAtTheEnd.get(indices.iterator().next()));
                } else {
                    //Actual merge process
                    MorphismRegister mergedMorphism = new MorphismRegister();
                    MultiMorphismRegister mergedMultimorphisms = new MultiMorphismRegister();
                    {
                        Map<IdExpr, NestedGraph.Reference> mergedMapsToMorphisms = new HashMap<>();
                        for (Integer j : indices) {
                            mergedMapsToMorphisms.putAll(mapMorphisms.get(j));
                            toMergeAtTheEnd.get(j).mergeMultismWith(mergedMultimorphisms);
                        }
                        mergedMapsToMorphisms.forEach((k,v) -> mergedMorphism.setRegister(k, v));
                    }
                    mergedStates.add(new TransformingState(mergedMorphism, mergedMultimorphisms, toSubst));
                }
            } else {
                IdExpr key = settedRegisters[i];
                HashMultimap<NestedGraph.Reference, Integer> toMerge = HashMultimap.create();
                for (Integer j: indices) {
                    toMerge.put(mapMorphisms.get(i).get(key), j);
                }
                Map<NestedGraph.Reference, Collection<Integer>> map = toMerge.asMap();
                for (Map.Entry<NestedGraph.Reference, Collection<Integer>> e : map.entrySet()) {
                    if (e.getValue().size() == 1) {
                        mergedStates.add(toMergeAtTheEnd.get(e.getValue().iterator().next()));
                        return;
                    } else {
                        merge(i+1, e.getValue());
                    }
                }
            }
        }

        public Set<TransformingState> doMerge(List<TransformingState> coll, PatternNGraph toSubst) {
            if (mergedStates == null) {
                mergedStates = new HashSet<>();
            } else
                mergedStates.clear();
            if (!coll.isEmpty()) {
                this.toSubst = toSubst;
                mapMorphisms = new ArrayList<>();
                Set<IdExpr> settedRegisters = new HashSet<>();
                int i = 0;
                indices = new HashSet<>();
                for (TransformingState x : coll) {
                    x.addMorphismToMapList(mapMorphisms);
                    settedRegisters.addAll(mapMorphisms.get(i).keySet());
                    indices.add(i++);
                }
                this.settedRegisters = settedRegisters.toArray(new IdExpr[settedRegisters.size()]);
                toMergeAtTheEnd = coll;
                mergedStates = new HashSet<>();
                merge(0, indices);
            }
            return mergedStates;
        }
    }

    public MorphismRegister() {
        state = new OptRef<>(MorphismList.create());
    }

    private MorphismRegister(OptRef<MorphismList> intermediateState) {
        state = intermediateState;
    }

    @Override
    public boolean setRegister(IdExpr predicateId, NestedGraph.Reference object) {
        if (isValidState()) {
            state = state.unsafeGet().addMorphismWith(object, predicateId);
            return isValidState();
        } else return false;
    }

    @Override
    public boolean isValidState() {
        return state.isPresent();
    }

    @Override
    public Set<IdExpr> returnSettedRegisters() {
        return state.isPresent() ? state.unsafeGet().morphismPredicates() : new HashSet<>();
    }

    @Override
    public Collection<NestedGraph.Reference> returnValues() {
        return state.isPresent() ? state.unsafeGet().morphismReferences() : new HashSet<>();
    }

    @Override
    public Iterator<NestedGraph.Reference> getRegister(IdExpr elementId) {
        return state.isPresent() ? state.unsafeGet().getMorphismOf(elementId) : new VoidIterator<>();
    }

    @Override
    public void fillFromRegisterOf(IdExpr id, Collection<IdExpr> resolveIdsWithPatterns) {
        if (state.isPresent()) {
            state.unsafeGet().instantiateMorphismOf(id,resolveIdsWithPatterns);
        }
    }

    @Override
    public void getVertexAndEdgeReferences(Collection<NestedGraph.Reference> vRef, Collection<NestedGraph.Reference> eRef) {
        if (state.isPresent()) state.unsafeGet().morphismReferences(vRef,eRef);
    }

    @Override
    public String detailedString() {
        return state.elim(MorphismList::detailedString, "");
    }

    @Override
    public MorphismRegister copy() {
        return new MorphismRegister(state);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MorphismRegister)) return false;

        MorphismRegister that = (MorphismRegister) o;

        return state.equals(that.state);
    }

    @Override
    public int hashCode() {
        return state.hashCode();
    }

    public boolean containsValue(NestedGraph.Reference dataDestinationVertex) {
        return state.isPresent() && state.unsafeGet().containsKey(dataDestinationVertex);
    }

    public boolean containsRegisterWithValue(IdExpr key, NestedGraph.Reference dataDestinationVertex) {
        return state.isPresent() && state.unsafeGet().containsKey(dataDestinationVertex, key);
    }

    @Override
    public String toString() {
        return state.toString();
    }
}
