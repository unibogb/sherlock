package it.giacomobergami.utils;

import java.util.Iterator;

/**
 * Created by vasistas on 14/08/16.
 */
public class VoidIterator<T> implements Iterator<T> {
    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public T next() {
        return null;
    }
}
