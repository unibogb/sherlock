package it.giacomobergami.operators;

import it.giacomobergami.nestedGraph.repository.NestedGraph;

@FunctionalInterface
public interface UnaryOperator {
    public NestedGraph apply(NestedGraph argument);
}
