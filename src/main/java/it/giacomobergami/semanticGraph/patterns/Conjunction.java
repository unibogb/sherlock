package it.giacomobergami.semanticGraph.patterns;

import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.nestedGraph.repository.TransformNGraph;
import it.giacomobergami.nestedGraph.utils.JPair;

import java.util.function.Predicate;

public class Conjunction extends JPair<PatternNGraph, TransformNGraph> {
    private Conjunction(PatternNGraph key, TransformNGraph value) {
        super(key, value);
    }
    public static Conjunction create() {
        PatternNGraph head = new PatternNGraph();

        Predicate<NestedGraph.Reference> headOfConjunction =
                new PossibleEdgeLabels(false,"cc").withOutEdgeLabel("conj");

        Predicate<NestedGraph.Reference> conjunctionElement = new PossibleEdgeLabels(true,"cc");
        Predicate<NestedGraph.Reference> inConjunctionElement = new PossibleEdgeLabels(true,"conj");
        NestedGraph.Reference root = head.insertVertexQuery(headOfConjunction, true);
        NestedGraph.Reference conj = head.insertVertexQuery(conjunctionElement, true);
        NestedGraph.Reference elem = head.insertVertexQuery(inConjunctionElement, false);
        NestedGraph.Reference rcEdge = head.insertEdgeQuery(x -> x.solve().hasLabel("cc"), true, root, conj);
        head.insertEdgeQuery(x -> x.solve().hasLabel("conj"), false, root, elem);

        /*
         * MATCH
         */
        TransformNGraph tail = new TransformNGraph();
        NestedGraph.Reference hRoot = tail.holdVertex(root);
        NestedGraph.Reference hConj = tail.holdVertex(conj);
        NestedGraph.Reference hElem = tail.insertNewVertex();
        hElem.solve().setFromExternal("value","value", elem);
                //tail.holdVertex(elem);
        NestedGraph.Reference hRConj = tail.holdEdge(rcEdge);
        NestedGraph.Reference moveEdge = tail.insertNewEdge(hConj.id, hElem.id);


        return new Conjunction(head, tail);
    }
}
