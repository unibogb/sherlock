package it.giacomobergami.semanticGraph.patterns;

import it.giacomobergami.grammarSolver.MatchingQuery;
import it.giacomobergami.nestedGraph.queryInterpretation.Exists;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.nestedGraph.repository.TransformNGraph;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.utils.states.TransformingState;

import java.util.Collection;
import java.util.function.Predicate;

public class MoveDeterminerPattern extends JPair<PatternNGraph, TransformNGraph> {
    private MoveDeterminerPattern(PatternNGraph key, TransformNGraph value) {
        super(key, value);
    }
    public static MoveDeterminerPattern create() {

        PatternNGraph head = new PatternNGraph();
        Predicate<NestedGraph.Reference> vertexProp = x -> new Exists() {
            @Override
            public boolean testEdge(NestedGraph.Reference next) {
                return next.solve().hasLabel("det") || next.solve().hasLabel("aux");
            }
        }.test(x.outEdges());
        NestedGraph.Reference vqWithDet = head.insertVertexQuery(vertexProp, true);

        NestedGraph.Reference vqDet = head.insertVertexQuery(x -> true, false);
        head.insertEdgeQuery(x -> x.solve().hasLabel("det") || x.solve().hasLabel("aux"), false, vqWithDet, vqDet);

        /*
         * MATCH
         */
        TransformNGraph tail = new TransformNGraph();
        tail.holdVertex(vqWithDet).solve()
                .setFromExternal("det","value",vqDet);

        return new MoveDeterminerPattern(head, tail);
    }
}
