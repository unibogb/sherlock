package it.giacomobergami.semanticGraph.patterns;

import it.giacomobergami.nestedGraph.queries.QExpression;
import it.giacomobergami.nestedGraph.queryInterpretation.Exists;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.nestedGraph.repository.TransformNGraph;
import it.giacomobergami.nestedGraph.utils.JPair;

import java.util.function.Predicate;

public class ChangeWith extends JPair<PatternNGraph, TransformNGraph> {
    private ChangeWith(PatternNGraph key, TransformNGraph value) {
        super(key, value);
    }
    public static ChangeWith create() {

        PatternNGraph head = new PatternNGraph();
        Predicate<NestedGraph.Reference> vertexProp = x -> new Exists() {
            @Override
            public boolean testEdge(NestedGraph.Reference next) {
                return next.solve().hasLabel("case");
            }
        }.test(x.outEdges());

        NestedGraph.Reference vqWithDet = head.insertVertexQuery(vertexProp, true);
        NestedGraph.Reference vqDet = head.insertVertexQuery(x -> x.solve().get("value").key._case().equals(QExpression.Type.Value) && x.solve().get("value").key.toString().equals("with"), false);
        NestedGraph.Reference vqNext = head.insertVertexQuery(x -> true, true);
        NestedGraph.Reference keepEdge = head.insertEdgeQuery(x -> x.solve().hasLabel("case"), true, vqWithDet, vqDet);
        /*toremoveEdge*/ head.insertEdgeQuery(x -> true, true, vqWithDet, vqNext);
        head.addJoinCondition(vqDet, vqNext, (x,y) -> x.solve().get("id").key.numeric().equals(y.solve().get("id").key.numeric().add(1)));
        /*
         * MATCH
         */
        TransformNGraph tail = new TransformNGraph();
        tail.holdVertex(vqWithDet);
        NestedGraph.Reference src = tail.holdVertex(vqDet);
        NestedGraph.Reference dst = tail.holdVertex(vqNext);
        tail.insertNewEdgeWithForcedIndex(10,src.id, dst.id);

        return new ChangeWith(head, tail);
    }
}
