package it.giacomobergami.semanticGraph.patterns;

import it.giacomobergami.nestedGraph.queryInterpretation.Exists;
import it.giacomobergami.nestedGraph.repository.NestedGraph;

import java.util.function.Predicate;

public class PossibleEdgeLabels implements Predicate<NestedGraph.Reference> {

   private Predicate<NestedGraph.Reference> prop;
   private final boolean isIngoing;

   public PossibleEdgeLabels(boolean ingoing) {
       prop = x -> false;
       isIngoing = ingoing;
   }

   public PossibleEdgeLabels(boolean ingoing, final String firstLabel) {
       isIngoing = ingoing;
       prop = x -> new Exists() {
           @Override
           public boolean testEdge(NestedGraph.Reference next) {
               return next.solve().hasLabel(firstLabel);
           }
       }.test(isIngoing ? x.inEdges() : x.outEdges());
   }

   public PossibleEdgeLabels withOutEdgeLabel(final String label) {
       prop = prop.or(x -> new Exists() {
           @Override
           public boolean testEdge(NestedGraph.Reference next) {
               return next.solve().hasLabel(label);
           }
       }.test(isIngoing ? x.inEdges() : x.outEdges()));
       return this;
   }

    @Override
    public boolean test(NestedGraph.Reference reference) {
        return prop.test(reference);
    }
}
