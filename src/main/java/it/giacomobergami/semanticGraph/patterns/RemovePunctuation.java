package it.giacomobergami.semanticGraph.patterns;

import it.giacomobergami.nestedGraph.queryInterpretation.Exists;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.repository.PatternNGraph;
import it.giacomobergami.nestedGraph.repository.TransformNGraph;
import it.giacomobergami.nestedGraph.utils.JPair;

import java.util.function.Predicate;

public class RemovePunctuation extends JPair<PatternNGraph, TransformNGraph> {
    private RemovePunctuation(PatternNGraph key, TransformNGraph value) {
        super(key, value);
    }
    public static RemovePunctuation create() {
        PatternNGraph head = new PatternNGraph();
        Predicate<NestedGraph.Reference> vertexProp = x -> new Exists() {
            @Override
            public boolean testEdge(NestedGraph.Reference next) {
                return next.solve().hasLabel("punct");
            }
        }.test(x.outEdges());
        NestedGraph.Reference vqWithDet = head.insertVertexQuery(vertexProp, true);
        NestedGraph.Reference vqDet = head.insertVertexQuery(x -> true, false);
        head.insertEdgeQuery(x -> x.solve().hasLabel("punct"), false, vqWithDet, vqDet);

        /*
         * MATCH
         */
        TransformNGraph tail = new TransformNGraph();
        NestedGraph.Reference transformed = tail.holdVertex(vqWithDet);
        transformed.solve().setFromExternal("value","value",vqWithDet);

        return new RemovePunctuation(head, tail);
    }
}
