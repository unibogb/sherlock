package it.giacomobergami.semanticGraph.patterns;

import it.giacomobergami.nestedGraph.queryInterpretation.Exists;
import it.giacomobergami.nestedGraph.repository.NestedGraph;

import java.util.function.Predicate;

public class PossibleInLabels implements Predicate<NestedGraph.Reference> {

   private Predicate<NestedGraph.Reference> prop;

   public PossibleInLabels() {
       prop = x -> false;
   }

   public PossibleInLabels(final String firstLabel) {
       prop = x -> new Exists() {
           @Override
           public boolean testEdge(NestedGraph.Reference next) {
               return next.solve().hasLabel(firstLabel);
           }
       }.testEdge(x);
   }

   public PossibleInLabels withOutEdgeLabel(final String label) {
       prop = prop.or(x -> new Exists() {
           @Override
           public boolean testEdge(NestedGraph.Reference next) {
               return next.solve().hasLabel(label);
           }
       }.testEdge(x));
       return this;
   }

    @Override
    public boolean test(NestedGraph.Reference reference) {
        return prop.test(reference);
    }
}
