package it.giacomobergami.semanticGraph;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.CoreMap;
import it.giacomobergami.nestedGraph.HVertex;
import it.giacomobergami.nestedGraph.repository.NestedGraph;
import it.giacomobergami.nestedGraph.utils.JId;
import it.giacomobergami.nestedGraph.utils.JPair;
import it.giacomobergami.semanticGraph.edgeTypes.GetEdgeType;

import java.util.*;

public class StanfordGraph {
    NestedGraph graph;
    JId prevMaxVertex = new JId(0);
    JId prevMaxEdge = new JId(0);

    public StanfordGraph() {
        graph = new NestedGraph();
    }

    private JId getPakcedDataVertexId(int element) {
        return JId.dovetail(JId.ZERO, prevMaxVertex.copy().add(element));
    }
    private JId getNewPakcedDataEdgeId() {
        JId toret = JId.dovetail(JId.ZERO, prevMaxEdge);
        prevMaxEdge = prevMaxEdge.add(1);
        return toret;
    }
    public static JId getDataVertexId(JId element) {
        return JId.dovetail(JId.ZERO, element);
    }

    public static JId getDataVertexId(int element) {
        return JId.dovetail(JId.ZERO, new JId(element));
    }

    public StanfordGraph parse(String text) {
        text = text.toLowerCase();
        List<CoreMap> sentences = StanfordPipeline.annotate(text).get(CoreAnnotations.SentencesAnnotation.class);
        for(CoreMap sentence: sentences) {
            visit(sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class));
            prevMaxVertex = graph.maxVertexId();
        }
        return this;
    }

    public void visit(SemanticGraph g) {
        Set<Integer> visitedVertices = new HashSet<>();
        {
            Collection<IndexedWord> rootNodes = g.getRoots();
            Iterator<IndexedWord> nodes = rootNodes.iterator();
            while (nodes.hasNext()) {
                IndexedWord w = nodes.next();
                vertex(w, g, visitedVertices, true);
            }
        }
    }

    private void vertex(IndexedWord v, SemanticGraph g, Set<Integer> visitedVertices, boolean isRoot) {
        String value = v.value();
        String tag = v.tag();
        String stemmed = v.lemma();
        String nonWord = v.originalText();
        int index = v.get(CoreAnnotations.IndexAnnotation.class);
        visitedVertices.add(index);

        boolean hasWord = (!value.equals(tag));
        HVertex vertexIndex = graph.insertNewVertexWithForcedIndex(getPakcedDataVertexId(index)).solve();


        if (tag.equals("DT")) {
            tag = "det";
        } else if (tag.equals("EX")) {
            tag = "∃";
        } else if (tag.equals("JJ")) {
        } else if (tag.equals("JJR") || tag.equals("RBR")) {
            tag = "cmp";
        } else if (tag.equals("JJS") || tag.equals("RBS")) {
            tag = "aggregation";
        } else if (tag.equals("NNS")) {
            tag = "noun";
            vertexIndex.update("specification","common");
            vertexIndex.update("number","plural");
        } else if (tag.equals("NN")) {
            tag = "noun";
            vertexIndex.update("specification","common");
            vertexIndex.update("number","singular");
        } else if (tag.equals("NNP")) {
            tag = "noun";
            vertexIndex.update("specification","proper");
            vertexIndex.update("number","singular");
        } else if (tag.equals("NNPS")) {
            tag = "noun";
            vertexIndex.update("specification","proper");
            vertexIndex.update("number","plural");
        } else if (tag.equals("WDT")) {
            tag = "var";
        }
        JPair<String, String> cp = QuerySemantics.extended_semantics.resolve(tag, hasWord ? value : nonWord);
        tag = cp.key;
        value = cp.value;
        vertexIndex.addLabel(tag);
        if (isRoot) vertexIndex.addLabel("root");
        vertexIndex.update("value",value);
        if (!value.equals(stemmed)) vertexIndex.update("lemma",stemmed);
        vertexIndex.update("pos", index);

        for (SemanticGraphEdge e : g.outgoingEdgeList(v)) {
            edge(index, e, g, visitedVertices);
        }
    }

    private void edge(long srcId, SemanticGraphEdge edge, SemanticGraph g, Set<Integer> visitedVertices) {
        Integer targetId = edge.getTarget().get(CoreAnnotations.IndexAnnotation.class);
        //Checking if it has a case
        String role = GetEdgeType.getInstance().apply(edge).toString();

        //Getting Specific
        String shortName = edge.getRelation().getShortName();
        String[] ar = shortName.split(":");
        String specific = null;
        if (ar.length==2) {
            specific = ar[1];
        } else specific = edge.getRelation().getSpecific();

        HVertex edgeIndex =  graph.insertNewEdgeWithForcedIndex( getNewPakcedDataEdgeId(),
                getPakcedDataVertexId((int)srcId),
                getPakcedDataVertexId((int)targetId)).solve();
        edgeIndex.addLabel(role);
        if (specific != null)
            edgeIndex.update("specification",specific);

        if (!visitedVertices.contains(targetId)) {
            vertex(edge.getTarget(), g, visitedVertices, false);
        }
    }

    @Override
    public String toString() {
        return graph.toString();
    }

    public NestedGraph getGraph() {
        return graph;
    }
}
