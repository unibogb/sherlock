graph [
	node [
		id 1
		blueprintsId "than?[]"
	]
	node [
		id 2
		blueprintsId "authors?[]"
	]
	node [
		id 3
		blueprintsId "<?[]"
	]
	node [
		id 4
		blueprintsId "10?[]"
	]
	node [
		id 5
		blueprintsId "papers?[]"
	]
	node [
		id 6
		blueprintsId "published?[]"
	]
	node [
		id 7
		blueprintsId "return?[]"
	]
	node [
		id 8
		blueprintsId "all?[]"
	]
	node [
		id 9
		blueprintsId "the?[]"
	]
	node [
		id 10
		blueprintsId "with?[]"
	]
	edge [
		source 2
		target 7
		label "amod"
		blueprintsId "0"
	]
	edge [
		source 2
		target 8
		label "det:predet"
		blueprintsId "1"
	]
	edge [
		source 2
		target 9
		label "det"
		blueprintsId "2"
	]
	edge [
		source 2
		target 5
		label "nmod"
		blueprintsId "3"
	]
	edge [
		source 5
		target 10
		label "case"
		blueprintsId "4"
	]
	edge [
		source 5
		target 4
		label "nummod"
		blueprintsId "5"
	]
	edge [
		source 4
		target 3
		label "advmod"
		blueprintsId "6"
	]
	edge [
		source 3
		target 1
		label "mwe"
		blueprintsId "7"
	]
	edge [
		source 2
		target 6
		label "acl"
		blueprintsId "8"
	]
]
